import { useState, useEffect, FormEvent } from 'react'
import { useNavigate } from 'react-router-dom'
import { getAllRoles } from '../../services/rolesService'
import { IRol } from '../../interfaces/roleInterface'
import { checkLoggedUser } from '../../libs/checkLoggedUser'
import {
    IUserPost,
    initialUserPost
} from '../../interfaces/userInterface'
import { signIn } from '../../services/authService'
import toast from 'react-hot-toast'

import {
    Grid,
    TextField,
    Button,
    Paper,
    Autocomplete
} from '@mui/material'

const Login = () => {
    const [roles, setRoles] = useState<IRol[]>([])
    const [user, setUser] = useState<IUserPost>(initialUserPost)
    const navigate = useNavigate()

    const getRoles = async () => {
        const data = await getAllRoles()
        setRoles(data)
    }

    useEffect(() => {
        const { logged } = checkLoggedUser()
        if (!logged) {
            getRoles()
        } else {
            navigate("/dashboard")
        }
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        try {
            await signIn(user)
            navigate("/dashboard")
            toast.success('Inicio de sesión con éxito', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al iniciar sesión.', {
                position: 'top-center',
                duration: 5000
            })
        }

        onClickCancelar()
    }

    const onClickCancelar = () => {
        setUser(initialUserPost)
    }

    return (
        <>
        {roles.length > 0 ? (
            <Grid 
                container 
                spacing={3}
                sx={{
                    height: '100%',
                    minHeight: '100vh',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Grid item lg={3}>
                    <Paper
                        sx={{
                            p: 2,
                            display: 'flex',
                            justifyContent: 'center',
                            flexDirection: 'column',
                            minHeight: 220
                        }}
                    >
                        <form onSubmit={handleSubmit}>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <TextField
                                        type="email"
                                        label="Correo electrónico"
                                        placeholder="Ingresa tu correo electrónico"
                                        variant="standard"
                                        fullWidth
                                        value={user.email}
                                        onChange={({target}) => setUser({...user, email: target.value})}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        type="password"
                                        label="Contraseña"
                                        placeholder="Ingresa tu contraseña"
                                        variant="standard"
                                        fullWidth
                                        value={user.password}
                                        onChange={({target}) => setUser({...user, password: target.value})}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Autocomplete
                                        disablePortal
                                        options={roles.map((rol) => rol.name)}
                                        renderInput={(params) => <TextField {...params} label="Rol"/>}
                                        value={user.role}
                                        onChange={(event, role) => setUser({...user, role: role})}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <Button
                                        type="submit"
                                        color="primary"
                                        variant="contained"
                                        fullWidth
                                    >
                                        Iniciar
                                    </Button>
                                </Grid>
                                <Grid item xs={6}>
                                    <Button
                                        color="error"
                                        variant="contained"
                                        fullWidth
                                        onClick={onClickCancelar}
                                    >
                                        Cancelar
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>
                    </Paper>
                </Grid>
            </Grid>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default Login