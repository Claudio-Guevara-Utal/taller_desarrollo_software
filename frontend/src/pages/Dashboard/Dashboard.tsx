import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { checkLoggedUser } from '../../libs/checkLoggedUser'

import SectionRender from '../../components/SectionRender'
import SectionList from '../../components/SectionList'

import {
    ThemeProvider,
    createTheme,
    styled
} from '@mui/material/styles'
import {
    Box,
    CssBaseline,
    Toolbar,
    IconButton,
    Typography,
    List,
    ListItem,
    ListItemIcon,
    Avatar,
    Drawer,
    ListItemText,
    Divider
} from '@mui/material'
import MuiAppBar, { AppBarProps as MuiAppBarProps  } from '@mui/material/AppBar'
import Menu from '@mui/icons-material/Menu'
import ChevronLeft from '@mui/icons-material/ChevronLeft'
import Logout from '@mui/icons-material/Logout'

const mdTheme = createTheme()
const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
    open?: boolean;
}>(({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    backgroundColor: theme.palette.grey[300],
    height: '100%',
    minHeight: '100vh',
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    }),
}));

interface AppBarProps extends MuiAppBarProps {
    open?: boolean;
}
  
const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));
    

const Dashboard = () => {
    const [open, setOpen] = useState(false)
    const [roleName, setRoleName] = useState<string>('')
    const [section, setSection] = useState<string>('transaction')
    const navigate = useNavigate()

    useEffect(() => {
        const { logged, role } = checkLoggedUser()
        if (!logged) navigate("/")
        setRoleName(role)
    }, [])

    const toggleDrawer = () => {
        setOpen(!open)
    }

    const logout = () => {
        window.localStorage.removeItem('user')
        navigate("/")
    }

    return (
        <ThemeProvider theme={mdTheme}>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline/>
                <AppBar position="absolute" open={open}>
                    <Toolbar
                        sx={{
                            pr: '24px'
                        }}
                    >
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="open drawer"
                            onClick={toggleDrawer}
                            sx={{
                                marginRight: '36px',
                                ...(open && { display: 'none' }),
                            }}
                        >
                            <Menu/>
                        </IconButton>
                        <Typography
                            component="h1"
                            variant="h6"
                            color="inherit"
                            noWrap
                            sx={{ flexGrow: 1 }}
                        >
                            Clínica Bupa
                        </Typography>
                        <List>
                            <ListItem>
                                <ListItemIcon>
                                    <Avatar
                                        alt="Foto de perfil"
                                    />
                                </ListItemIcon>
                                <ListItemIcon>
                                    <IconButton onClick={logout}>
                                        <Logout/>
                                    </IconButton>
                                </ListItemIcon>
                            </ListItem>
                        </List>
                    </Toolbar>
                </AppBar>
                <Drawer
                    sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                    }}
                    variant="persistent"
                    anchor="left"
                    open={open}
                >
                    <DrawerHeader>
                        <List>
                            <ListItem>
                                <ListItemText primary={roleName}/>
                                <ListItemIcon>
                                    <IconButton onClick={toggleDrawer}>
                                        <ChevronLeft/>
                                    </IconButton>
                                </ListItemIcon>
                            </ListItem>
                        </List>
                    </DrawerHeader>
                    <Divider/>
                    <SectionList setSection={setSection} roleName={roleName}/>
                </Drawer> 
                <Main open={open}>
                    <DrawerHeader/>
                    <SectionRender section={section} setSection={setSection}/>
                </Main>
            </Box>
        </ThemeProvider>
    )
}

export default Dashboard