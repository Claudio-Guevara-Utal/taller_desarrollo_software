import axios from "axios";
import { ISolutionTypePost } from '../interfaces/solutionTypeInterface'

const baseUrl = 'http://localhost:4000/api/solution-types'

export const createSolutionType = async (newSolutionType: ISolutionTypePost) => {
    return await axios.post(baseUrl, newSolutionType)
}

export const getAllSolutionTypes = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const deleteSolutionType = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateSolutionType = async (id: string, updatedSolutionType: ISolutionTypePost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedSolutionType)
}