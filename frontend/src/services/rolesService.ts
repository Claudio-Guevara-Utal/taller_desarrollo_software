import axios from 'axios'

const baseUrl = 'http://localhost:4000/api/roles'

export const getAllRoles = async () => {
    const { data } = await axios.get(baseUrl)
    return data
} 