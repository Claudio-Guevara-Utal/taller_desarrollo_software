import axios from 'axios'
import {
    ILaboratoryEquipmentPost
} from '../interfaces/laboratoryEquipmentInterface'

const baseUrl = 'http://localhost:4000/api/laboratory-equipments'

export const getAllLaboratoryEquipments = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const createLaboratoryEquipment = async (newLaboratoryEquipment: ILaboratoryEquipmentPost) => {
    return await axios.post(baseUrl, newLaboratoryEquipment)
}

export const deleteLaboratoryEquipment = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateLaboratoryEquipment = async (id: string, updatedLaboratoryEquipment: ILaboratoryEquipmentPost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedLaboratoryEquipment)
}