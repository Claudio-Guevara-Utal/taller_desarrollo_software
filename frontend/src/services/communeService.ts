import axios from 'axios'
import { ICommunePost } from '../interfaces/communeInterface'

const baseUrl = 'http://localhost:4000/api/communes'

export const getAllCommunes = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const createCommune = async (newCommune: ICommunePost) => {
    return await axios.post(baseUrl, newCommune)
}

export const deleteCommune = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateCommune = async (id: string, updatedCommune: ICommunePost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedCommune)
}