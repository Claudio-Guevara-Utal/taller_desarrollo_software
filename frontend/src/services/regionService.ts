import axios from "axios";
import { IRegionPost } from '../interfaces/regionInterface'

const baseUrl = 'http://localhost:4000/api/regions'

export const createRegion = async (newRegion: IRegionPost) => {
    return await axios.post(baseUrl, newRegion)
}

export const getAllRegions = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const deleteRegion = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateRegion = async (id: string, updatedRegion: IRegionPost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedRegion)
}