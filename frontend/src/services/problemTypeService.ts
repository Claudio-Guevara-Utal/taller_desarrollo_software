import axios from 'axios'
import { IProblemTypePost } from '../interfaces/problemTypeInterface'

const baseUrl = 'http://localhost:4000/api/problem-types'

export const createProblemType = async (newProblemType: IProblemTypePost) => {
    return await axios.post(baseUrl, newProblemType)
}

export const getAllProblemTypes = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const deleteProblemType = async (id: string)  => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateProblemType = async (id: string, udpatedProblemType: IProblemTypePost) => {
    return await axios.put(`${baseUrl}/${id}`, udpatedProblemType)
}