import axios from "axios";
import { IProviderPost } from '../interfaces/providerInterface'

const baseUrl = 'http://localhost:4000/api/providers'

export const createProvider = async (newProvider: IProviderPost) => {
    return await axios.post(baseUrl, newProvider)
}

export const getAllProviders = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const deleteProvider = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateProvider = async (id: string, updatedProvider: IProviderPost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedProvider)
}