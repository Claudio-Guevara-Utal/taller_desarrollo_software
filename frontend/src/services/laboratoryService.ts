import axios from 'axios'
import { ILaboratoryPost } from '../interfaces/laboratoryInterface'

const baseUrl = 'http://localhost:4000/api/laboratories'

export const createLaboratory = async (newLaboratory: ILaboratoryPost) => {
    return await axios.post(baseUrl, newLaboratory)
}

export const getAllLaboratories = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const deleteLaboratory = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateLaboratory = async (id: string, updatedLaboratory: ILaboratoryPost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedLaboratory)
}