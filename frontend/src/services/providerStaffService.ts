import axios from 'axios'

const baseUrl = 'http://localhost:4000/api/providers-staff'

export const getAllProvidersStaffEmail = async () => {
    const { data } = await axios.get(`${baseUrl}/emails`)
    return data
}