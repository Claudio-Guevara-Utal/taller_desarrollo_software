import axios from 'axios'
import { ITransactionPost } from '../interfaces/transactionInterface'

const baseUrl = 'http://localhost:4000/api/transactions'

export const getAllTransactions = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const createTransaction = async (newTransaction: ITransactionPost) => {
    return await axios.post(baseUrl, newTransaction)
}

export const deleteTransaction = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateTransaction = async (id: string, updatedTransaction: ITransactionPost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedTransaction)
}