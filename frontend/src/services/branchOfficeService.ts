import axios from 'axios'
import { IBranchOfficePost } from '../interfaces/branchOfficeInterface'

const baseUrl = 'http://localhost:4000/api/branch-offices'

export const getAllBranchOffices = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const createBranchOffice = async (newBranchOffice: IBranchOfficePost) => {
    return await axios.post(baseUrl, newBranchOffice)
}

export const deleteBranchOffice = async (id: string) => {
    return await axios.delete(`${baseUrl}/${id}`)
}

export const updateBranchOffice = async (id: string, updatedBranchOffice: IBranchOfficePost) => {
    return await axios.put(`${baseUrl}/${id}`, updatedBranchOffice)
}