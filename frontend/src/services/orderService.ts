import axios from "axios";
import { IOrderStaffPost } from '../interfaces/orderInterface'

const baseUrl = 'http://localhost:4000/api/orders'

export const getAllOrders = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const assignProvidersStaff = async (orderID: string, problemID: string, providersStaff: string[], staffAssignmentDate: string) => {
    return await axios.post(`${baseUrl}/assign`, {orderID, problemID, providersStaff, staffAssignmentDate})
}

export const getAllOrdersStaff = async () => {
    const { data } = await axios.get(`${baseUrl}/staffs`)
    return data
}

export const updateSolutionProblem = async (problemID: string, orderStaff: IOrderStaffPost) => {
    const { solutionDate, solutionType } = orderStaff
    return await axios.post(`${baseUrl}/solution`, { problemID, solutionDate, solutionType })
}