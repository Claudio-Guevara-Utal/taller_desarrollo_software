import axios from 'axios'
import jwt_decode from 'jwt-decode'
import { IUserPost, IUserGet } from '../interfaces/userInterface'
import { IUserRegisterPost } from '../interfaces/userRegisterInterface'

const baseUrl = 'http://localhost:4000/api/auth'

export const signIn = async (user: IUserPost) => {
    const { data } =  await axios.post(`${baseUrl}/signin`, user)
    const { token } = data
    const decoded: IUserGet = jwt_decode(token)

    const userInformation = {
        token: token,
        name: decoded.name,
        lastname: decoded.lastname,
        email: decoded.email,
        role: decoded.role
    }

    window.localStorage.setItem('user', JSON.stringify(userInformation))
}

export const signUp = async (newUser: IUserRegisterPost) => {
    return await axios.post(`${baseUrl}/signup`, newUser)
}