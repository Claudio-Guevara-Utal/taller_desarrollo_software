import axios from "axios";
import { IProblemPost } from '../interfaces/problemInterface'

const baseUrl = 'http://localhost:4000/api/problems'

export const createProblem = async (newProblem: IProblemPost) => {
    return await axios.post(baseUrl, newProblem)
}

export const getAllProblems = async () => {
    const { data } = await axios.get(baseUrl)
    return data
}

export const getProblemsOfOneTransaction = async (transactionID: string) => {
    const { data } = await axios.post(`${baseUrl}/one-transaction`, { transactionID })
    return data
}