export interface IProviderPost {
    name: string
    email: string
    direction: string
    commune: string | null
}

export interface IProviderGet {
    _id: string
    name: string
    email: string
    direction: string
    commune: {
        name: string
    }
    createdAt: string
    updatedAt: string
}

export const initialStateProvider: IProviderPost = {
    name: '',
    email: '',
    direction: '',
    commune: ''
}