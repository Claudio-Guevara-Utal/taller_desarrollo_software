export interface IOrderGet {
    _id: string
    transaction: {
        laboratoryEquipment: {
            name: string
        }
        provider: {
            name: string
        }
        laboratory: {
            branchOffice: {
                name: string
            }
        }
    }
    problem: {
        _id: string
        problemType: {
            name: string
        }
        problemDescription: string
        problemDate: string
        solved: boolean
    }
    providersStaff: { email: string }[]
    createdAt: string
    updatedAt: string
    staffAssignmentDate: string
}

export interface IOrderStaffGet {
    _id: string
    transaction: {
        laboratoryEquipment: {
            name: string
        }
        laboratory: {
            name: string
            branchOffice: {
                name: string
                direction: string
                commune: {
                    name: string
                    region: {
                        name: string
                    }
                }
            }
        }
        provider: {
            name: string
        }
    }
    problem: {
        _id: string
        solved: boolean
        problemType: {
            name: string
        }
    }
    providersStaff: { email: string }[]
    createdAt: string
    updatedAt: string
    staffAssignmentDate: string
}

export interface IOrderStaffPost {
    solutionDate: Date | null
    solutionType: string | null
}

export const initialStateOrderStaff: IOrderStaffPost = {
    solutionDate: new Date(),
    solutionType: ''
}