export interface IUserPost {
    email: string
    password: string
    role: string | null
}

export interface IUserGet {
    id: string
    name: string
    lastname: string
    email: string
    role: string
    exp: number
    iat: number
}

export const initialUserPost: IUserPost = {
    email: '',
    password: '',
    role: ''
}