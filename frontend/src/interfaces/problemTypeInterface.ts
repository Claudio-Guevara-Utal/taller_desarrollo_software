export interface IProblemTypePost {
    name: string
}

export interface IProblemTypeGet {
    _id: string
    name: string
    createdAt: string
    updatedAt: string
}

export const initialStateProblemType: IProblemTypePost = {
    name: ''
}