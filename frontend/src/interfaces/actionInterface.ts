export interface IAction {
    type: string
    id: string
}

export const initialStateAction: IAction = {
    type: 'Crear',
    id: ''
}