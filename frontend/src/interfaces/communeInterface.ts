interface IRegion {
    name: string
}

export interface ICommuneGet {
    _id: string
    name: string
    region: IRegion
    createdAt: string
    updatedAt: string
}

export interface ICommunePost {
    name: string
    region: string | null
}

export const initialStateCommune: ICommunePost = {
    name: '',
    region: ''
}

