export interface ILaboratoryEquipmentGet {
    _id: string
    name: string
    function: string
}

export interface ILaboratoryEquipmentPost {
    name: string
    function: string
}

export const initialStateLaboratoryEquipment: ILaboratoryEquipmentPost = {
    name: '',
    function: ''
}