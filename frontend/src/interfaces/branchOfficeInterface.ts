interface ICommune {
    name: string
}

export interface IBranchOfficeGet {
    _id: string
    name: string
    direction: string
    commune: ICommune
    operative: boolean
    createdAt: string
    updatedAt: string
}

export interface IBranchOfficePost {
    name: string
    direction: string
    commune: string | null
    operative: boolean
}

export const initialStateBranchOffice: IBranchOfficePost = {
    name: '',
    direction: '',
    commune: '',
    operative: true
}