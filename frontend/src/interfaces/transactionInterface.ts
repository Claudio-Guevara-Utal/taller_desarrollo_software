export interface ITransactionPost {
    laboratoryEquipment: string | null
    laboratory: string | null
    provider: string | null 
} 

export interface ITransactionGet {
    _id: string
    laboratoryEquipment: {
        name: string
    }
    laboratory: {
        name: string
        branchOffice: {
            name: string
        }
    }
    provider: {
        name: string
    }
}

export const initialStateTransaction: ITransactionPost = {
    laboratoryEquipment: '',
    laboratory: '',
    provider: ''
}