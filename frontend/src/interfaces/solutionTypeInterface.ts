export interface ISolutionTypePost {
    name: string
}

export interface ISolutionTypeGet {
    _id: string
    name: string
    createdAt: string
    updatedAt: string
}

export const initialStateSolutionType: ISolutionTypePost = {
    name: ''
}