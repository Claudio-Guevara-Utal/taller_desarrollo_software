export interface IUserRegisterPost {
    name: string
    lastname: string
    rut: string
    email: string
    role: string | null
}

export const initialStateUserRegister: IUserRegisterPost = {
    name: '',
    lastname: '',
    rut: '',
    email: '',
    role: ''
}