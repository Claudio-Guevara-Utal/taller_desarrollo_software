export interface IRegionPost {
    _id?: string
    name: string
    createdAt?: string
    updatedAt?: string
}

export interface IRegionGet {
    _id: string
    name: string
    createdAt: string
    updatedAt: string
}

export const initialStateRegion: IRegionPost = {
    name: ''
}