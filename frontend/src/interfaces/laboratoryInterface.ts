interface IBranchOffice {
    name: string
}

export interface ILaboratoryPost {
    name: string
    branchOffice: string | null
}

export interface ILaboratoryGet {
    _id: string
    name: string
    branchOffice: IBranchOffice
    createdAt: string
    updatedAt: string
}

export const initialStateLaboratory: ILaboratoryPost = {
    name: '',
    branchOffice: ''
}