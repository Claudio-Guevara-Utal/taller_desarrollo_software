export interface IProblemPost {
    problemType: string | null
    problemDescription: string
    problemDate: Date | null
    transactionID?: string
}

export interface IProblemGet {
    _id: string
    problemType: {
        name: string
    }
    problemDescription: string
    problemDate: string
    solved: boolean
    transaction: {
        laboratoryEquipment: {
            name: string
        }
        laboratory: {
            branchOffice: {
                name: string
            }
        }
        provider: {
            name: string
        }
    }
    providersStaff: { email: string }[]
    createdAt: string
    updatedAt: string
    staffAssignmentDate: string
    solutionDate: string
    solutionType: {
        name: string
    }
}

export const initialStateProblem: IProblemPost = {
    problemType: '',
    problemDescription: '',
    problemDate: new Date(),
    answerType: '',
    answerDate: new Date(),
    answerDescription: '',
}