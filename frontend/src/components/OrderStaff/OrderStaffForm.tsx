import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import { ISolutionTypeGet } from '../../interfaces/solutionTypeInterface'
import { IOrderStaffPost, initialStateOrderStaff } from '../../interfaces/orderInterface'
import * as solutionTypeService from '../../services/solutionTypeService'
import * as orderService from '../../services/orderService'
import toast from 'react-hot-toast'
import { 
    Grid,
    Autocomplete,
    TextField,
    Button
} from '@mui/material'
import Close from '@mui/icons-material/Close'
import {
    DateTimePicker,
    LocalizationProvider
} from '@mui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'

interface Props {
    orderStaff: IOrderStaffPost
    setOrderStaff: Dispatch<SetStateAction<IOrderStaffPost>>
    handleClose: () => void
    problemID: string
    updateListOrdersStaff: boolean
    setUpdateListOrdersStaff: Dispatch<SetStateAction<boolean>>
}

const OrderStaffForm = (props: Props) => {
    const {
        handleClose,
        orderStaff,
        setOrderStaff,
        problemID,
        updateListOrdersStaff,
        setUpdateListOrdersStaff
    } = props
    const [solutionTypes, setSolutionTypes] = useState<ISolutionTypeGet[]>([])

    const getSolutionTypes = async () => {
        try {  
            const data = await solutionTypeService.getAllSolutionTypes()
            setSolutionTypes(data)
        } catch (error) {
            toast.error('Error al listar los tipos de soluciones.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getSolutionTypes()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        try {   
            await orderService.updateSolutionProblem(problemID, orderStaff)
            setUpdateListOrdersStaff(!updateListOrdersStaff)
            toast.success('Solución añadida con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al añadir la solución.', {
                position: 'top-center',
                duration: 5000
            })
        }
        onClickCancelar()
    }
    
    const onClickCancelar = () => {
        setOrderStaff(initialStateOrderStaff)
    }

    return (
        <>
        {solutionTypes.length > 0 ? (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid container justifyContent="flex-end" direction="row">
                        <Button color="primary"><Close onClick={handleClose}/></Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            options={solutionTypes.map((solution) => solution.name)}
                            renderInput={(params) => <TextField {...params} required label="Tipo de Solución"/>}
                            value={orderStaff.solutionType}
                            onChange={(event, solutionType) => setOrderStaff({...orderStaff, solutionType: solutionType})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DateTimePicker
                                label="Fecha de la solución"
                                renderInput={(params) => <TextField {...params}/>}
                                value={orderStaff.solutionDate}
                                onChange={(newDate) => setOrderStaff({...orderStaff, solutionDate: newDate})}
                            />
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            color="primary"
                            variant="contained"
                            fullWidth
                        >
                            Asignar
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default OrderStaffForm