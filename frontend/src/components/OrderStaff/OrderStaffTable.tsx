import { IOrderStaffGet } from '../../interfaces/orderInterface'
import DataTable from '../DataTable'
import { useState, useEffect } from 'react'
import HorizontalRule from '@mui/icons-material/HorizontalRule'
import QuestionMark from '@mui/icons-material/QuestionMark'
import AddTask from '@mui/icons-material/AddTask'
import { Button } from '@mui/material' 
import TrafficLight from '../TrafficLight'

const columns: string[] = ["Técnicos", "Fecha de Asignación", "Equipo", "Problema", "Sucursal", "Dirección", "Comuna", "Región", "Estado", "Opciones"]
type DataTableType = any[]

interface Props {
    ordersStaff: IOrderStaffGet[]
    handleOpen: (problemID: string) => void
}

const OrderStaffTable = (props: Props) => {
    const {
        ordersStaff,
        handleOpen
    } = props
    const [ordersStaffData, setOrdersStaffData] = useState<DataTableType>([])

    const dateFormat = (date: string) => {
        const newDate = date.split("T")[0]
        return newDate
    }

    const providersFormat = (providers: { email: string }[]) => {
        let cadena = ''
        providers.map((provider, key) => {
            if (key + 1 < providers.length) {
                cadena = cadena + provider.email + ' - '
            } else {
                cadena = cadena + provider.email
            }
        })
        return cadena
    }

    const dataTransform = async () => {
        let data: DataTableType = []
        ordersStaff.map((order) => {
            if (order.providersStaff.length > 0) {
                let colour 
                if (!order.problem) {
                    colour = ''
                } else if (order.problem.solved) {
                    colour = 'green'
                } else if (order.problem.solved === false && order.providersStaff.length > 0) {
                    colour = 'yellow'
                } else if (order.problem.solved === false && order.providersStaff.length === 0) {
                    colour = 'red'
                }

                data.push([
                    order.providersStaff ? providersFormat(order.providersStaff) : <HorizontalRule/>,
                    order.staffAssignmentDate ? dateFormat(order.staffAssignmentDate) : <QuestionMark/>,
                    order.transaction ? order.transaction.laboratoryEquipment ? order.transaction.laboratoryEquipment.name : <HorizontalRule/> : <HorizontalRule/>,
                    order.problem ? order.problem.problemType ? order.problem.problemType.name : <HorizontalRule/> : <HorizontalRule/>,
                    order.transaction ? order.transaction.laboratory ? order.transaction.laboratory.branchOffice ? order.transaction.laboratory.branchOffice.name : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/>,
                    order.transaction ? order.transaction.laboratory ? order.transaction.laboratory.branchOffice ? order.transaction.laboratory.branchOffice.direction : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/>,
                    order.transaction ? order.transaction.laboratory ? order.transaction.laboratory.branchOffice ? order.transaction.laboratory.branchOffice.commune ? order.transaction.laboratory.branchOffice.commune.name : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/>,
                    order.transaction ? order.transaction.laboratory ? order.transaction.laboratory.branchOffice ? order.transaction.laboratory.branchOffice.commune ? order.transaction.laboratory.branchOffice.commune.region ? order.transaction.laboratory.branchOffice.commune.region.name : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/>,
                    <TrafficLight colour={colour}/>,
                    <Button onClick={() => handleOpen(order.problem._id)} color="info"><AddTask/></Button>
                ])
            }
        })
        setOrdersStaffData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [ordersStaff])

    return (
        <>
        {ordersStaffData.length > 0 ? (
            <DataTable
                title="Pedidos pendientes"
                columns={columns}
                data={ordersStaffData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default OrderStaffTable