import { useState, useEffect } from 'react'
import { IOrderStaffGet, IOrderStaffPost, initialStateOrderStaff } from '../../interfaces/orderInterface'
import * as orderService from '../../services/orderService'
import toast from 'react-hot-toast'
import OrderStaffTable from './OrderStaffTable'
import OrderStaffModal from './OrderStaffModal'
import { Grid, Typography } from '@mui/material'

const OrderStaff = () => {
    const [ordersStaff, setOrdersStaff] = useState<IOrderStaffGet[]>([])
    const [updateListOrdersStaff, setUpdateListOrdersStaff] = useState<boolean>(true)
    const [open, setOpen] = useState<boolean>(false)
    const [problemID, setProblemID] = useState<string>('')
    const [orderStaff, setOrderStaff] = useState<IOrderStaffPost>(initialStateOrderStaff)

    const handleOpen = (problemID: string): void => {
        setOpen(true)
        setProblemID(problemID)
    }
    const handleClose = (): void => setOpen(false)

    const getOrdersStaff = async () => {
        try {
            const data = await orderService.getAllOrdersStaff()
            setOrdersStaff(data)
        } catch (error) {
            toast.error('Error al listar los pedidos pendientes.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getOrdersStaff()
    }, [updateListOrdersStaff])

    return (
        <>
        {ordersStaff.length > 0 ? (
            <Grid container spacing={3}>
                <Grid item lg={12}>
                    <Typography variant="h4">Pedidos pendientes</Typography>
                </Grid>
                <Grid item lg={12}>
                    <OrderStaffTable
                        ordersStaff={ordersStaff}
                        handleOpen={handleOpen}
                    />
                </Grid>
                <Grid item lg={6}>
                    <OrderStaffModal
                        open={open}
                        handleClose={handleClose}
                        problemID={problemID}
                        updateListOrdersStaff={updateListOrdersStaff}
                        setUpdateListOrdersStaff={setUpdateListOrdersStaff}
                        orderStaff={orderStaff}
                        setOrderStaff={setOrderStaff}
                    />
                </Grid>
            </Grid>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default OrderStaff