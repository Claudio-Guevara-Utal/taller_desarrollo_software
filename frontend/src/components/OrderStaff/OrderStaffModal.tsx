import { Dispatch, SetStateAction } from 'react'
import { Grid } from '@mui/material'
import { styled, Box } from '@mui/system'
import { ModalUnstyled } from '@mui/core'
import OrderStaffForm from './OrderStaffForm'
import { IOrderStaffPost } from '../../interfaces/orderInterface'

const StyledModal = styled(ModalUnstyled)`
position: fixed;
z-index: 1300;
right: 0;
bottom: 0;
top: 0;
left: 0;
display: flex;
align-items: center;
justify-content: center;
`;

const style = {
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  p: 4,
  px: 4,
  pb: 3,
};

interface Props {
    open: boolean
    handleClose: () => void
    problemID: string
    updateListOrdersStaff: boolean
    setUpdateListOrdersStaff: Dispatch<SetStateAction<boolean>>
    orderStaff: IOrderStaffPost
    setOrderStaff: Dispatch<SetStateAction<IOrderStaffPost>>
}

const OrderStaffModal = (props: Props) => {
    const {
        open,
        handleClose,
        problemID,
        updateListOrdersStaff,
        setUpdateListOrdersStaff,
        orderStaff,
        setOrderStaff
    } = props

    return (
        <Grid container justifyContent="flex-end" direction="row">
            <StyledModal
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <OrderStaffForm
                        handleClose={handleClose}
                        orderStaff={orderStaff}
                        setOrderStaff={setOrderStaff}
                        problemID={problemID}
                        updateListOrdersStaff={updateListOrdersStaff}
                        setUpdateListOrdersStaff={setUpdateListOrdersStaff}
                    />
                </Box>
            </StyledModal>
        </Grid>
    )
}

export default OrderStaffModal