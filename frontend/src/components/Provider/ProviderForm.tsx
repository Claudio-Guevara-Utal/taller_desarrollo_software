import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import { ICommuneGet } from '../../interfaces/communeInterface'
import { IProviderPost, initialStateProvider } from '../../interfaces/providerInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import * as communeService from '../../services/communeService'
import * as providerService from '../../services/providerService'
import toast from 'react-hot-toast'
import {
    Paper,
    TextField,
    Autocomplete,
    Button,
    Grid
} from '@mui/material'

interface Props {
    provider: IProviderPost
    setProvider: Dispatch<SetStateAction<IProviderPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListProviders: boolean
    setUpdateListProviders: Dispatch<SetStateAction<boolean>>
}

const ProviderForm = (props: Props) => {
    const {
        provider,
        setProvider,
        action,
        setAction,
        updateListProviders,
        setUpdateListProviders
    } = props
    const [communes, setCommunes] = useState<ICommuneGet[]>([])

    const getCommunes = async () => {
        try {
            const data = await communeService.getAllCommunes()
            setCommunes(data)
        } catch (error) {
            toast.error('Error al listar las comunas.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getCommunes()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await providerService.createProvider(provider)
                setUpdateListProviders(!updateListProviders)
                toast.success('Proveedor creado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al crear el proveedor.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        } else if (action.type === 'Editar') {
            try {
                await providerService.updateProvider(action.id, provider)
                setUpdateListProviders(!updateListProviders)
                toast.success('Proveedor actualizado con éxito', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al actualizar el proveedor.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setProvider(initialStateProvider)
        setAction(initialStateAction)
    }

    return (
        <>
        {communes.length > 0 ? (
            <Paper
                sx={{
                    p: 2,
                    display: 'flex',
                    justifyContent: 'center',
                    flexDirection: 'column'
                }}
            >
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <TextField
                                label="Nombre"
                                placeholder="Nombre del proveedor"
                                required
                                autoFocus
                                fullWidth
                                value={provider.name}
                                onChange={({target}) => setProvider({...provider, name: target.value})}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Email"
                                placeholder="Correo electrónico"
                                required
                                fullWidth
                                type="email"
                                value={provider.email}
                                onChange={({target}) => setProvider({...provider, email: target.value})}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Dirección"
                                placeholder="Dirección"
                                required
                                fullWidth
                                value={provider.direction}
                                onChange={({target}) => setProvider({...provider, direction: target.value})}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Autocomplete
                                disablePortal
                                options={communes.map((commune) => commune.name)}
                                renderInput={(params) => <TextField {...params} required label="Comuna"/>}
                                value={provider.commune}
                                onChange={(event, commune) => setProvider({...provider, commune: commune})}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                type="submit"
                                color="primary"
                                variant="contained"
                                fullWidth
                            >
                                {action.type}
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                color="error"
                                variant="contained"
                                fullWidth
                                onClick={onClickCancelar}
                            >
                                Cancelar
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default ProviderForm