import DataTable from '../DataTable'
import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { IProviderGet, IProviderPost } from '../../interfaces/providerInterface'
import { IAction } from '../../interfaces/actionInterface'
import HorizontalRule from '@mui/icons-material/HorizontalRule'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import { Button } from '@mui/material'
import * as providerService from '../../services/providerService'
import toast from 'react-hot-toast'

const columns: string[] = ["Nombre", "Email", "Dirección", "Comuna", "Opciones"]
type DataTableType = [string, string, string, string | any, any[]][]

interface Props {
    providers: IProviderGet[]
    setProvider: Dispatch<SetStateAction<IProviderPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListProviders: boolean
    setUpdateListProviders: Dispatch<SetStateAction<boolean>>
}

const ProviderTable = (props: Props) => {
    const {
        providers,
        setProvider,
        setAction,
        updateListProviders,
        setUpdateListProviders
    } = props
    const [providersData, setProvidersData] = useState<DataTableType>([])

    const dataTransform = () => {
        let data: DataTableType = []
        providers.map((provider) => {
            data.push([
                provider.name, 
                provider.email, 
                provider.direction, 
                provider.commune ? provider.commune.name : <HorizontalRule/>,
                [<Button onClick={() => updateProvider(provider)} color="warning"><Edit/></Button>, <Button onClick={() => deleteProvider(provider._id)} color="error"><Delete/></Button>]
            ])
        })
        setProvidersData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [providers])

    const updateProvider = (provider: IProviderGet) => {
        setProvider({
            name: provider.name,
            email: provider.email,
            direction: provider.direction,
            commune: provider.commune ? provider.commune.name : ''
        })
        setAction({
            type: 'Editar',
            id: provider._id
        })
    }

    const deleteProvider = async (id: string) => {
        try {
            await providerService.deleteProvider(id)
            setUpdateListProviders(!updateListProviders)
            toast.success('Proveedor eliminado con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al eliminar el proveedor.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {providersData.length > 0 ? (
            <DataTable
                title="Lista de proveedores"
                columns={columns}
                data={providersData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default ProviderTable