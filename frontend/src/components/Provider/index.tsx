import { useState } from 'react'
import { Grid, Typography } from '@mui/material'
import ProviderForm from './ProviderForm'
import ProviderList from './ProviderList'
import { IProviderPost, initialStateProvider } from '../../interfaces/providerInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

const Provider = () => {
    const [provider, setProvider] = useState<IProviderPost>(initialStateProvider)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [updateListProviders, setUpdateListProviders] = useState<boolean>(true)

    return (
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Typography variant="h4">Proveedores</Typography>
            </Grid>
            <Grid item lg={3}>
                <ProviderForm
                    action={action}
                    provider={provider}
                    setAction={setAction}
                    setProvider={setProvider}
                    setUpdateListProviders={setUpdateListProviders}
                    updateListProviders={updateListProviders}
                />
            </Grid>
            <Grid item lg={8}>
                <ProviderList
                    setAction={setAction}
                    setProvider={setProvider}
                    setUpdateListProviders={setUpdateListProviders}
                    updateListProviders={updateListProviders}
                />
            </Grid>
        </Grid>
    )
}

export default Provider