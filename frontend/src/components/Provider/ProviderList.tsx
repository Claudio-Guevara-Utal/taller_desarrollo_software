import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import * as providerService from '../../services/providerService'
import { IProviderGet, IProviderPost } from '../../interfaces/providerInterface'
import { IAction } from '../../interfaces/actionInterface'
import toast from 'react-hot-toast'
import ProviderTable from './ProviderTable'

interface Props {
    setProvider: Dispatch<SetStateAction<IProviderPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListProviders: boolean
    setUpdateListProviders: Dispatch<SetStateAction<boolean>>
}

const ProviderList = (props: Props) => {
    const {
        setProvider,
        setAction,
        updateListProviders,
        setUpdateListProviders
    } = props
    const [providers, setProviders] = useState<IProviderGet[]>([])

    const getProviders = async () => {
        try {
            const data = await providerService.getAllProviders()
            setProviders(data)
        } catch (error) {
            toast.error('Error al listar los proveedores.', {
                position: 'top-center',
                duration: 5000
            })
        }
    } 

    useEffect(() => {
        getProviders()
    }, [updateListProviders])

    return (
        <>
        {providers.length > 0 ? (
            <ProviderTable
                providers={providers}
                setAction={setAction}
                setProvider={setProvider}
                setUpdateListProviders={setUpdateListProviders}
                updateListProviders={updateListProviders}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default ProviderList