import { useState } from 'react'
import { Grid, Typography } from '@mui/material'
import BranchOfficeModal from './BranchOfficeModal'
import BranchOfficeList from './BranchOfficeList'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import { IBranchOfficePost, initialStateBranchOffice } from '../../interfaces/branchOfficeInterface'

const BranchOffice = () => {
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [branchOffice, setBranchOffice] = useState<IBranchOfficePost>(initialStateBranchOffice)
    const [open, setOpen] = useState<boolean>(false)
    const [updateListBranchOffices, setUpdateListBranchOffices] = useState<boolean>(true)

    const handleOpen = (): void => setOpen(true)
    const handleClose = (): void => setOpen(false)

    return (
        <Grid container spacing={3}>
            <Grid item lg={6}>
                <Typography variant="h4">Sucursales</Typography>
            </Grid>
            <Grid item lg={6}>
                <BranchOfficeModal
                    action={action}
                    setAction={setAction}
                    branchOffice={branchOffice}
                    setBranchOffice={setBranchOffice}
                    open={open}
                    handleOpen={handleOpen}
                    handleClose={handleClose}
                    updateListBranchOffices={updateListBranchOffices}
                    setUpdateListBranchOffices={setUpdateListBranchOffices}
                />
            </Grid>
            <Grid item lg={12}>
                <BranchOfficeList
                    setAction={setAction}
                    setBranchOffice={setBranchOffice}
                    setUpdateListBranchOffices={setUpdateListBranchOffices}
                    updateListBranchOffices={updateListBranchOffices}
                    setOpen={setOpen}
                />
            </Grid>
        </Grid>
    )
}

export default BranchOffice