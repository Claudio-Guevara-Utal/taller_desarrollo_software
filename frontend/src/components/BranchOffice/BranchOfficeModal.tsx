import { Dispatch, SetStateAction } from 'react'
import { Grid, Button } from '@mui/material'
import { styled, Box } from '@mui/system'
import { ModalUnstyled } from '@mui/core'
import BranchOfficeForm from './BranchOfficeForm'
import { IBranchOfficePost } from '../../interfaces/branchOfficeInterface'
import { IAction } from '../../interfaces/actionInterface'
import Add from '@mui/icons-material/Add'

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const style = {
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
    px: 4,
    pb: 3,
};

interface Props {
    open: boolean
    handleOpen: () => void
    handleClose: () => void
    branchOffice: IBranchOfficePost
    setBranchOffice: Dispatch<SetStateAction<IBranchOfficePost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListBranchOffices: boolean
    setUpdateListBranchOffices: Dispatch<SetStateAction<boolean>>
}

const BranchOfficeModal = (props: Props) => {
    const {
        open,
        handleOpen,
        handleClose,
        branchOffice,
        setBranchOffice,
        action,
        setAction,
        updateListBranchOffices,
        setUpdateListBranchOffices
    } = props

    return (
        <Grid container justifyContent="flex-end" direction="row">
            <Button
                type="button"
                variant="contained"
                color="info"
                onClick={handleOpen}
            >
                <Add/><span style={{ marginLeft: 5 }}>Añadir Sucursal</span>
            </Button>
            <StyledModal
                aria-labelledby="unstyled-modal-title"
                aria-describedby="unstyled-modal-description"
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <BranchOfficeForm
                        handleClose={handleClose}
                        branchOffice={branchOffice}
                        setBranchOffice={setBranchOffice}
                        action={action}
                        setAction={setAction}
                        updateListBranchOffices={updateListBranchOffices}
                        setUpdateListBranchOffices={setUpdateListBranchOffices}
                    />
                </Box>
            </StyledModal>
        </Grid>
    )
}

export default BranchOfficeModal