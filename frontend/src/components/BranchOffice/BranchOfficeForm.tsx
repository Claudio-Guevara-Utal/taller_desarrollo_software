import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import {
    Grid,
    TextField,
    Autocomplete,
    FormControlLabel,
    Checkbox,
    Button
} from '@mui/material'
import Close from '@mui/icons-material/Close'
import { ICommuneGet } from '../../interfaces/communeInterface'
import { IBranchOfficePost, initialStateBranchOffice } from '../../interfaces/branchOfficeInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import * as communeService from '../../services/communeService'
import * as branchOfficeService from '../../services/branchOfficeService'
import toast from 'react-hot-toast'

interface Props {
    handleClose: () => void
    branchOffice: IBranchOfficePost
    setBranchOffice: Dispatch<SetStateAction<IBranchOfficePost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListBranchOffices: boolean
    setUpdateListBranchOffices: Dispatch<SetStateAction<boolean>>
}

const BranchOfficeForm = (props: Props) => {
    const {
        handleClose,
        branchOffice,
        setBranchOffice,
        action,
        setAction,
        updateListBranchOffices,
        setUpdateListBranchOffices
    } = props
    const [communes, setCommunes] = useState<ICommuneGet[]>([])

    const getCommunes = async () => {
        try {
            const data = await communeService.getAllCommunes()
            setCommunes(data)
        } catch (error) {
            toast.error('Eror al listar las comunas.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getCommunes()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await branchOfficeService.createBranchOffice(branchOffice)
                toast.success('Sucursal creada con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
                setUpdateListBranchOffices(!updateListBranchOffices)
            } catch (error) {
                toast.error('Error al crear la sucursal.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        } else if (action.type === 'Editar') {
            try {
                await branchOfficeService.updateBranchOffice(action.id, branchOffice)
                setUpdateListBranchOffices(!updateListBranchOffices)
                toast.success('Sucursal actualizada con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al actualizar la sucursal.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setBranchOffice(initialStateBranchOffice)
        setAction(initialStateAction)
    }

    const resetModal = () => {
        onClickCancelar()
        handleClose()
    }

    return (    
        <>
        {communes.length > 0 ? (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid container justifyContent="flex-end" direction="row">
                        <Button color="primary"><Close onClick={resetModal}/></Button>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Nombre"
                            placeholder="Nombre de la sucursal"
                            fullWidth
                            required
                            autoFocus
                            value={branchOffice.name}
                            onChange={({target}) => setBranchOffice({...branchOffice, name: target.value})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Dirección"
                            placeholder="Dirección de la sucursal"
                            fullWidth
                            required
                            value={branchOffice.direction}
                            onChange={({target}) => setBranchOffice({...branchOffice, direction: target.value})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            options={communes.map((commune) => commune.name)}
                            renderInput={(params) => <TextField {...params} required label="Comuna"/>}
                            value={branchOffice.commune}
                            onChange={(event, commune) => setBranchOffice({...branchOffice, commune: commune})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormControlLabel 
                            label="Operativa"
                            control={
                                <Checkbox 
                                    checked={branchOffice.operative} 
                                    onChange={({target}) => setBranchOffice({...branchOffice, operative: target.checked})}
                                />
                            }
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            fullWidth
                        >
                            {action.type}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default BranchOfficeForm