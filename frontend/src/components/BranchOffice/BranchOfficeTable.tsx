import { useEffect, useState, Dispatch, SetStateAction } from 'react'
import DataTable from '../DataTable'
import { IBranchOfficeGet, IBranchOfficePost } from '../../interfaces/branchOfficeInterface'
import { IAction } from '../../interfaces/actionInterface'
import HorizonalRule from '@mui/icons-material/HorizontalRule'
import Check from '@mui/icons-material/Check'
import HighlightOff from '@mui/icons-material/HighlightOff'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import { Button } from '@mui/material'
import * as branchOfficeService from '../../services/branchOfficeService'
import toast from 'react-hot-toast'

type DataTableType = [string, string, string | any, any, any][]
const columns: string[] = ["Nombre", "Dirección", "Comuna", "Operativa", "Opciones"] 

interface Props {
    branchOffices: IBranchOfficeGet[]
    setBranchOffice: Dispatch<SetStateAction<IBranchOfficePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListBranchOffices: boolean
    setUpdateListBranchOffices: Dispatch<SetStateAction<boolean>>
    setOpen: Dispatch<SetStateAction<boolean>>
}

const BranchOfficeTable = (props: Props) => {
    const { 
        branchOffices,
        setBranchOffice,
        setAction,
        updateListBranchOffices,
        setUpdateListBranchOffices,
        setOpen
    } = props
    const [branchOfficesData, setBranchOfficesData] = useState<DataTableType>([])

    const dataTransform = () => {
        let data: DataTableType = []
        branchOffices.map((branchOffice) => {
            data.push([
                branchOffice.name, 
                branchOffice.direction,
                branchOffice.commune ? branchOffice.commune.name : <HorizonalRule/>,
                branchOffice.operative ? <Check/> : <HighlightOff/>,
                [<Button onClick={() => updateBranchOffice(branchOffice)} color="warning"><Edit/></Button>, <Button onClick={() => deleteBranchOffice(branchOffice._id)} color="error"><Delete/></Button>]
            ])
            setBranchOfficesData(data)
        })
    }

    useEffect(() => {
        dataTransform()
    }, [branchOffices])

    const updateBranchOffice = (branchOffice: IBranchOfficeGet) => {
        setBranchOffice({
            name: branchOffice.name,
            direction: branchOffice.direction,
            commune: branchOffice.commune ? branchOffice.commune.name : '',
            operative: branchOffice.operative
        })
        setAction({
            type: 'Editar',
            id: branchOffice._id
        })
        setOpen(true)
    }

    const deleteBranchOffice = async (id: string) => {
        try {
            await branchOfficeService.deleteBranchOffice(id)
            setUpdateListBranchOffices(!updateListBranchOffices)
            toast.success('Sucursal eliminada con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al eliminar el sucursal.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {branchOfficesData.length > 0 ? (
            <DataTable
                title="Lista de sucursales"
                columns={columns}
                data={branchOfficesData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default BranchOfficeTable