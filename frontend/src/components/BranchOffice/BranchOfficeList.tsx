import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import BranchOfficeTable from './BranchOfficeTable'
import { IBranchOfficeGet, IBranchOfficePost } from '../../interfaces/branchOfficeInterface'
import { IAction } from '../../interfaces/actionInterface'
import * as branchOfficeService from '../../services/branchOfficeService'
import toast from 'react-hot-toast'

interface Props {
    setBranchOffice: Dispatch<SetStateAction<IBranchOfficePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListBranchOffices: boolean
    setUpdateListBranchOffices: Dispatch<SetStateAction<boolean>>
    setOpen: Dispatch<SetStateAction<boolean>>
}

const BranchOfficeList = (props: Props) => {
    const {
        setBranchOffice,
        setAction,
        updateListBranchOffices,
        setUpdateListBranchOffices,
        setOpen
    } = props
    const [branchOffices, setBranchOffices] = useState<IBranchOfficeGet[]>([])

    const getBranchOffices = async () => {
        try {
            const data = await branchOfficeService.getAllBranchOffices()
            setBranchOffices(data)
        } catch (error) {
            toast.error('Error al listar las sucursales.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getBranchOffices()
    }, [updateListBranchOffices])

    return (
        <>
        {branchOffices.length > 0 ? (
            <BranchOfficeTable
                branchOffices={branchOffices}
                setAction={setAction}
                setBranchOffice={setBranchOffice}
                setUpdateListBranchOffices={setUpdateListBranchOffices}
                updateListBranchOffices={updateListBranchOffices}
                setOpen={setOpen}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default BranchOfficeList