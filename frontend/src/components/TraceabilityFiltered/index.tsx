import { Grid, Button } from '@mui/material'
import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { IProblemGet } from '../../interfaces/problemInterface'
import * as problemService from '../../services/problemService'
import toast from 'react-hot-toast'
import TraceabilityFilteredTable from './TraceabilityFilteredTable'
import ArrowBack from  '@mui/icons-material/ArrowBack'

interface Props {
    transactionID: string
    setSection: Dispatch<SetStateAction<string>>
}

const TraceabilityFiltered = (props: Props) => {
    const { transactionID, setSection } = props
    const [problems, setProblems] = useState<IProblemGet[]>([])

    const getProblems = async () => {
        try {
            const data = await problemService.getProblemsOfOneTransaction(transactionID)
            setProblems(data)
        } catch (error) {
            toast.error('Error al listar los problemas.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getProblems()
    }, [])

    return (
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={() => setSection('transaction')}
                >
                    <ArrowBack/><span style={{ marginLeft:5 }}>Regresar</span>
                </Button>
            </Grid>
            <Grid item lg={12}>
                <TraceabilityFilteredTable problems={problems}/>
            </Grid>
        </Grid>
    )
}

export default TraceabilityFiltered