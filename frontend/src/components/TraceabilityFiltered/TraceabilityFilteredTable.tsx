import { IProblemGet } from '../../interfaces/problemInterface'
import DataTable from '../DataTable'
import { useState, useEffect } from 'react'
import QuestionMark from '@mui/icons-material/QuestionMark'
import TrafficLight from '../TrafficLight'

interface Props {
    problems: IProblemGet[]
}

const columns: string[] = ["Equipo", "Sucursal", "Problema", "Fecha del Problema", "Proveedor", "Técnicos", "Fecha de Asignación", "Solución", "Fecha de la Solución", "Estado"]
type DataTableType = any[]

const TraceabilityFilteredTable = (props: Props) => {
    const { problems } = props
    const [problemsData, setProblemsData] = useState<DataTableType>([])

    const dateFormat = (date: string) => {
        const newDate = date.split("T")[0]
        return newDate
    }

    const providersFormat = (providers: { email: string }[]) => {
        let cadena = ''
        providers.map((provider, key) => {
            if (key + 1 < providers.length) {
                cadena = cadena + provider.email + ' - '
            } else {
                cadena = cadena + provider.email
            }
        })
        return cadena
    }

    const dataTransform = async () => {
        let data: DataTableType = []
        problems.map((problem) => {
            let colour 
            if (problem.solved) {
                colour = 'green'
            } else if (problem.solved === false && problem.providersStaff.length > 0) {
                colour = 'yellow'
            } else if (problem.solved === false && problem.providersStaff.length === 0) {
                colour = 'red'
            }

            data.push([
                problem.transaction ? problem.transaction.laboratoryEquipment ? problem.transaction.laboratoryEquipment.name : <QuestionMark/> : <QuestionMark/>,
                problem.transaction ? problem.transaction.laboratory ? problem.transaction.laboratory.branchOffice ? problem.transaction.laboratory.branchOffice.name : <QuestionMark/> : <QuestionMark/> : <QuestionMark/>,
                problem.problemType ? problem.problemType.name : <QuestionMark/>,
                problem.problemDate ?  dateFormat(problem.problemDate) : <QuestionMark/>,
                problem.transaction ? problem.transaction.provider ? problem.transaction.provider.name : <QuestionMark/> : <QuestionMark/>,
                problem.providersStaff.length > 0 ? providersFormat(problem.providersStaff) : <QuestionMark/>,
                problem.staffAssignmentDate ? dateFormat(problem.staffAssignmentDate) : <QuestionMark/>,
                problem.solutionType ? problem.solutionType.name : <QuestionMark/>,
                problem.solutionDate ? dateFormat(problem.solutionDate) : <QuestionMark/>,
                <TrafficLight colour={colour}/>
            ])
        })
        setProblemsData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [problems])

    return (
        <>
        {problemsData.length > 0 ? (
            <DataTable
                title="Trazabilidad"
                columns={columns}
                data={problemsData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default TraceabilityFilteredTable