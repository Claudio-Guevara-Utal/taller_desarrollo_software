import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import DataTable from '../DataTable'
import { IProblemTypeGet, IProblemTypePost } from '../../interfaces/problemTypeInterface'
import { IAction } from '../../interfaces/actionInterface'
import { Button } from '@mui/material'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import * as problemTypeService from '../../services/problemTypeService'
import toast from 'react-hot-toast'

const columns: string[] = ["Nombre", "Opciones"]
type DataTableType = [string, any[]][]

interface Props {
    problemTypes: IProblemTypeGet[]
    setProblemType: Dispatch<SetStateAction<IProblemTypePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListProblemTypes: boolean
    setUpdateListProblemTypes: Dispatch<SetStateAction<boolean>>
}

const ProblemTypeTable = (props: Props) => {
    const {
        problemTypes,
        setProblemType,
        setAction,
        updateListProblemTypes,
        setUpdateListProblemTypes
    } = props
    const [problemTypesData, setProblemTypesData] = useState<DataTableType>([])

    const dataTransform = () => {
        let data: DataTableType = []
        problemTypes.map((problemType) => {
            data.push([
                problemType.name, 
                [<Button onClick={() => updateProblemType(problemType)} color="warning"><Edit/></Button>, <Button onClick={() => deleteProblemType(problemType._id)} color="error"><Delete/></Button>]
            ])
        })
        setProblemTypesData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [problemTypes])

    const updateProblemType = (problemType: IProblemTypeGet) => {
        setProblemType({
            name: problemType.name
        })
        setAction({
            type: 'Editar',
            id: problemType._id
        })
    }

    const deleteProblemType = async (id: string) => {
        try {
            await problemTypeService.deleteProblemType(id)
            setUpdateListProblemTypes(!updateListProblemTypes)
            toast.success('Tipo de problema eliminado con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al eliminar el tipo de problema.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {problemTypesData.length > 0 ? (
            <DataTable
                title="Lista de los tipos de problemas"
                columns={columns}
                data={problemTypesData}
            />
        ) : (
            'cargando..'
        )}
        </>
    )
}

export default ProblemTypeTable