import { Grid, Typography } from '@mui/material'
import { useState } from 'react'
import { IProblemTypePost, initialStateProblemType } from '../../interfaces/problemTypeInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

import ProblemTypeForm from './ProblemTypeForm'
import ProblemTypeList from './ProblemTypeList'

const ProblemType = () => {
    const [problemType, setProblemType] = useState<IProblemTypePost>(initialStateProblemType)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [updateListProblemTypes, setUpdateListProblemTypes] = useState<boolean>(true) 

    return (
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Typography variant="h4">Tipos de problemas</Typography>
            </Grid>
            <Grid item lg={3}>
                <ProblemTypeForm
                    action={action}
                    problemType={problemType}
                    setAction={setAction}
                    setProblemType={setProblemType}
                    setUpdateListProblemTypes={setUpdateListProblemTypes}
                    updateListProblemTypes={updateListProblemTypes}
                />
            </Grid>
            <Grid item lg={5}>
                <ProblemTypeList
                    setAction={setAction}
                    setProblemType={setProblemType}
                    setUpdateListProblemTypes={setUpdateListProblemTypes}
                    updateListProblemTypes={updateListProblemTypes}
                />
            </Grid>
        </Grid>
    )
}

export default ProblemType