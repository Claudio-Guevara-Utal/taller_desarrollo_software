import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { IProblemTypeGet, IProblemTypePost } from '../../interfaces/problemTypeInterface'
import { IAction } from '../../interfaces/actionInterface'
import ProblemTypeTable from './ProblemTypeTable'
import { getAllProblemTypes } from '../../services/problemTypeService'
import toast from 'react-hot-toast'

interface Props {
    setProblemType: Dispatch<SetStateAction<IProblemTypePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListProblemTypes: boolean
    setUpdateListProblemTypes: Dispatch<SetStateAction<boolean>>
}

const ProblemTypeList = (props: Props) => {
    const {
        setProblemType,
        setAction,
        updateListProblemTypes,
        setUpdateListProblemTypes
    } = props
    const [problemTypes, setProblemTypes] = useState<IProblemTypeGet[]>([])

    const getProblemTypes = async () => {
        try {
            const data = await getAllProblemTypes()
            setProblemTypes(data)
        } catch (error) {
            toast.error('Error al listar los tipos de problemas.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getProblemTypes()
    }, [updateListProblemTypes])

    return (
        <>
        {problemTypes.length > 0 ? (
            <ProblemTypeTable
                problemTypes={problemTypes}
                setAction={setAction}
                setProblemType={setProblemType}
                updateListProblemTypes={updateListProblemTypes}
                setUpdateListProblemTypes={setUpdateListProblemTypes}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default ProblemTypeList