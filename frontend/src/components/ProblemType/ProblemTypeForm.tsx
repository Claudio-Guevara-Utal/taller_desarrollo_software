import {
    Paper,
    Grid,
    TextField,
    Button
} from '@mui/material'
import { Dispatch, FormEvent, SetStateAction } from 'react'
import { IProblemTypePost, initialStateProblemType } from '../../interfaces/problemTypeInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import * as problemTypeService from '../../services/problemTypeService'
import toast from 'react-hot-toast'

interface Props {
    problemType: IProblemTypePost
    setProblemType: Dispatch<SetStateAction<IProblemTypePost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListProblemTypes: boolean
    setUpdateListProblemTypes: Dispatch<SetStateAction<boolean>>
}

const ProblemTypeForm = (props: Props) => {
    const {
        problemType,
        setProblemType,
        action,
        setAction,
        updateListProblemTypes,
        setUpdateListProblemTypes
    } = props

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await problemTypeService.createProblemType(problemType)
                setUpdateListProblemTypes(!updateListProblemTypes)
                toast.success('Tipo de problema creado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al crear el tipo de problema.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        } else if (action.type === 'Editar') {
            try {
                await problemTypeService.updateProblemType(action.id, problemType)
                setUpdateListProblemTypes(!updateListProblemTypes)
                toast.success('Tipo de problema actualizado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al actualizar el tipo de problema.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setProblemType(initialStateProblemType)
        setAction(initialStateAction)
    }

    return (
        <Paper
            sx={{
                p: 2,
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column'
            }}
        >
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            label="Tipo de Problema"
                            placeholder="Nombre"
                            autoFocus
                            required
                            fullWidth
                            value={problemType.name}
                            onChange={({target}) => setProblemType({...problemType, name: target.value})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            color="primary"
                            variant="contained"
                            fullWidth
                        >
                            {action.type}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    )
}

export default ProblemTypeForm