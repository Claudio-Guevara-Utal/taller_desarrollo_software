import { Grid, Typography } from '@mui/material'
import UserModal from './UserModal'
import { useState } from 'react'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import { IUserRegisterPost, initialStateUserRegister } from '../../interfaces/userRegisterInterface'

const User = () => {
    const [open, setOpen] = useState<boolean>(false)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [userRegister, setUserRegister] = useState<IUserRegisterPost>(initialStateUserRegister) 

    const handleOpen = (): void => setOpen(true)
    const handleClose = (): void => setOpen(false) 

    return (
        <Grid container spacing={3}>
            <Grid item lg={6}>
                <Typography variant="h4">Usuarios</Typography>
            </Grid>
            <Grid item lg={6}>
                <UserModal
                    open={open}
                    handleOpen={handleOpen}
                    handleClose={handleClose}
                    action={action}
                    setAction={setAction}
                    setUserRegister={setUserRegister}
                    userRegister={userRegister}
                />
            </Grid>
        </Grid>
    )
}

export default User