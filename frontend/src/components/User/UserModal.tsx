import { 
    Grid, 
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Stack
} from '@mui/material'
import { styled, Box } from '@mui/system'
import { ModalUnstyled } from '@mui/core'
import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { IRol } from '../../interfaces/roleInterface'
import { IAction } from '../../interfaces/actionInterface'
import { IUserRegisterPost } from '../../interfaces/userRegisterInterface'
import * as roleService from '../../services/rolesService'
import toast from 'react-hot-toast'

import UserForm from './UserForm'

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const style = {
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
    px: 4,
    pb: 3,
};

interface Props {
    open: boolean
    handleOpen: () => void
    handleClose: () => void
    userRegister: IUserRegisterPost
    setUserRegister: Dispatch<SetStateAction<IUserRegisterPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
}

const UserModal = (props: Props) => {
    const {
        open,
        handleOpen,
        handleClose,
        userRegister,
        setUserRegister,
        action,
        setAction
    } = props
    const [roles, setRoles] = useState<IRol[]>([])
    const [defaultRole, setDefaultRole] = useState<string>('')

    const getRoles = async () => {
        try {
            const data = await roleService.getAllRoles()
            setDefaultRole(data[0].name)
            setRoles(data)
        } catch (error) {
            toast.error('Error al listar los roles.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getRoles()
    }, [])

    return (
        <>
        {roles.length > 0 ? (
            <Grid container justifyContent="flex-end" direction="row">
                <Stack direction="row" spacing={2}>
                    <Button
                        type="button"
                        variant="contained"
                        color="info"
                        fullWidth
                        onClick={handleOpen}
                    >
                        CREAR USUARIO
                    </Button>
                    {/* <FormControl fullWidth variant="filled">
                        <InputLabel id="role-select-label">Rol</InputLabel>
                        <Select 
                            labelId="role-select-label"
                            label="Rol"
                            value={defaultRole}
                            defaultValue={defaultRole}
                        >
                            {roles.map((role) => (
                                <MenuItem value={role.name}>{role.name}</MenuItem>
                            ))}
                        </Select>
                    </FormControl> */}
                </Stack>
                <StyledModal
                    aria-labelledby="unstyled-modal-title"
                    aria-describedby="unstyled-modal-description"
                    open={open}
                    onClose={handleClose}
                >
                    <Box sx={style}>
                        <UserForm
                            handleClose={handleClose}
                            action={action}
                            setAction={setAction}
                            setUserRegister={setUserRegister}
                            userRegister={userRegister}
                        />
                    </Box>
                </StyledModal>
            </Grid>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default UserModal