import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import { IRol } from '../../interfaces/roleInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import { initialStateUserRegister, IUserRegisterPost } from '../../interfaces/userRegisterInterface'
import * as roleService from '../../services/rolesService'
import toast from 'react-hot-toast'
import {
    Grid,
    TextField,
    Autocomplete,
    Button
} from '@mui/material'
import Close from '@mui/icons-material/Close'
import * as authService from '../../services/authService'

interface Props {
    handleClose: () => void
    userRegister: IUserRegisterPost
    setUserRegister: Dispatch<SetStateAction<IUserRegisterPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
}

const UserForm = (props: Props) => {
    const {
        handleClose,
        userRegister,
        setUserRegister,
        action,
        setAction
    } = props
    const [roles, setRoles] = useState<IRol[]>([])

    const getRoles = async () => {
        try {
            const data = await roleService.getAllRoles()
            setRoles(data)
        } catch (error) {
            toast.error('Error al listar los roles.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getRoles()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await authService.signUp(userRegister)
                toast.success('Usuario creado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al crear el usuario.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setUserRegister(initialStateUserRegister)
        setAction(initialStateAction)
    }

    const resetModal = () => {
        handleClose()
        onClickCancelar()
    }

    return (
        <>
        {roles.length > 0 ? (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid container justifyContent="flex-end" direction="row">
                        <Button onClick={resetModal}>
                            <Close/>
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Nombre"
                            placeholder="Nombre"
                            required
                            autoFocus
                            fullWidth
                            value={userRegister.name}
                            onChange={({target}) => setUserRegister({...userRegister, name: target.value})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Apellido"
                            placeholder="Apellido"
                            required
                            fullWidth
                            value={userRegister.lastname}
                            onChange={({target}) => setUserRegister({...userRegister, lastname: target.value})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Rut"
                            placeholder="xx.xxx.xxx-x"
                            required
                            fullWidth
                            value={userRegister.rut}
                            onChange={({target}) => setUserRegister({...userRegister, rut: target.value})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Email"
                            placeholder="Correo electrónico"
                            required
                            fullWidth
                            type="email"
                            value={userRegister.email}
                            onChange={({target}) => setUserRegister({...userRegister, email: target.value})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            options={roles.map((role) => role.name)}
                            renderInput={(params) => <TextField {...params} required label="Rol"/>}
                            value={userRegister.role}
                            onChange={(event, role) => setUserRegister({...userRegister, role: role})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            color="primary"
                            variant="contained"
                            fullWidth
                        >
                            {action.type}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default UserForm