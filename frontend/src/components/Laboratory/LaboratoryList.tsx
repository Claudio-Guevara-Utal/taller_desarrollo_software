import LaboratoryTable from './LaboratoryTable'
import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { ILaboratoryGet, ILaboratoryPost } from '../../interfaces/laboratoryInterface'
import { IAction } from '../../interfaces/actionInterface' 
import * as laboratoryService from '../../services/laboratoryService'
import toast from 'react-hot-toast'

interface Props {
    setLaboratory: Dispatch<SetStateAction<ILaboratoryPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListLaboratories: boolean
    setUpdateListLaboratories: Dispatch<SetStateAction<boolean>>
}

const LaboratoryList = (props: Props) => {
    const {
        setLaboratory,
        setAction,
        updateListLaboratories,
        setUpdateListLaboratories
    } = props
    const [laboratories, setLaboratories] = useState<ILaboratoryGet[]>([])

    const getLaboratories = async () => {
        try {
            const data = await laboratoryService.getAllLaboratories()
            setLaboratories(data)
        } catch (error) {
            toast.error('Error al listar los laboratorios.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getLaboratories()
    }, [updateListLaboratories])

    return (
        <>
        {laboratories.length > 0 ? (
            <LaboratoryTable
                laboratories={laboratories}
                setAction={setAction}
                setLaboratory={setLaboratory}
                setUpdateListLaboratories={setUpdateListLaboratories}
                updateListLaboratories={updateListLaboratories}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default LaboratoryList