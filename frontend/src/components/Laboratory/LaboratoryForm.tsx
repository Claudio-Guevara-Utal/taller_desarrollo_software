import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import { ILaboratoryPost, initialStateLaboratory } from '../../interfaces/laboratoryInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import { IBranchOfficeGet } from '../../interfaces/branchOfficeInterface'
import * as branchOfficeService from '../../services/branchOfficeService'
import {
    Paper,
    Grid,
    TextField,
    Autocomplete,
    Button
} from '@mui/material'
import * as laboratoryService from '../../services/laboratoryService'
import toast from 'react-hot-toast'

interface Props {
    laboratory: ILaboratoryPost
    setLaboratory: Dispatch<SetStateAction<ILaboratoryPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListLaboratories: boolean
    setUpdateListLaboratories: Dispatch<SetStateAction<boolean>>
}

const LaboratoryForm = (props: Props) => {
    const {
        laboratory,
        setLaboratory,
        action,
        setAction,
        updateListLaboratories,
        setUpdateListLaboratories
    } = props
    const [branchOffices, setBranchOffices] = useState<IBranchOfficeGet[]>([])

    const getBranchOffices = async () => {
        try {
            const data = await branchOfficeService.getAllBranchOffices()
            setBranchOffices(data)
        } catch (error) {
            toast.error('Error al listar las sucursales.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getBranchOffices()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await laboratoryService.createLaboratory(laboratory)
                setUpdateListLaboratories(!updateListLaboratories)
                toast.success('Laboratorio creado con éxito', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al crear al laboratorio.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        } else if (action.type === 'Editar') {
            try {
                await laboratoryService.updateLaboratory(action.id, laboratory)
                setUpdateListLaboratories(!updateListLaboratories)
                toast.success('Laboratorio actualizado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al actualizar el laboratorio.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setLaboratory(initialStateLaboratory)
        setAction(initialStateAction)
    }

    return (
        <>
        {branchOffices.length > 0 ? (
            <Paper
                sx={{
                    p: 2,
                    display: 'flex',
                    justifyContent: 'center',
                    flexDirection: 'column'
                }}
            >
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <TextField
                                label="Laboratorio"
                                placeholder="Nombre del laboratorio"
                                required
                                autoFocus
                                fullWidth
                                value={laboratory.name}
                                onChange={({target}) => setLaboratory({...laboratory, name: target.value})}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Autocomplete
                                disablePortal
                                options={branchOffices.map((branchOffice) => branchOffice.name)}
                                renderInput={(params) => <TextField {...params} required label="Sucursal"/>}
                                value={laboratory.branchOffice}
                                onChange={(event, branchOffice) => setLaboratory({...laboratory, branchOffice: branchOffice})}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                fullWidth
                            >
                                {action.type}
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                variant="contained"
                                color="error"
                                fullWidth
                                onClick={onClickCancelar}
                            >
                                Cancelar
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default LaboratoryForm