import DataTable from '../DataTable'
import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { ILaboratoryGet, ILaboratoryPost } from '../../interfaces/laboratoryInterface'
import { IAction } from '../../interfaces/actionInterface'
import HorizontalRule from '@mui/icons-material/HorizontalRule'
import { Button } from '@mui/material'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import * as laboratoryService from '../../services/laboratoryService'
import toast from 'react-hot-toast'

type DataTableType = [string, string | any, any[]][]
const columns: string[] = ["Nombre", "Sucursal", "Opciones"]

interface Props {
    laboratories: ILaboratoryGet[]
    setLaboratory: Dispatch<SetStateAction<ILaboratoryPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListLaboratories: boolean
    setUpdateListLaboratories: Dispatch<SetStateAction<boolean>>
}

const LaboratoryTable = (props: Props) => {
    const {
        laboratories,
        setLaboratory,
        setAction,
        updateListLaboratories,
        setUpdateListLaboratories
    } = props
    const [laboratoriesData, setLaboratoriesData] = useState<DataTableType>([])

    const dataTransform = () => {
        let data: DataTableType = []
        laboratories.map((laboratory) => {
            data.push([
                laboratory.name,
                laboratory.branchOffice ? laboratory.branchOffice.name : <HorizontalRule/>,
                [<Button onClick={() => updateLaboratory(laboratory)} color="warning"><Edit/></Button>, <Button onClick={() => deleteLaboratory(laboratory._id)} color="error"><Delete/></Button>]
            ])
        })
        setLaboratoriesData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [laboratories])

    const updateLaboratory = (laboratory: ILaboratoryGet) => {
        setLaboratory({
            name: laboratory.name,
            branchOffice: laboratory.branchOffice ? laboratory.branchOffice.name : ''
        })
        setAction({
            type: 'Editar',
            id: laboratory._id
        })
    }

    const deleteLaboratory = async (id: string) => {
        try {
            await laboratoryService.deleteLaboratory(id)
            toast.success('Laboratorio eliminado con éxito.', {
                position: 'top-center',
                duration: 5000
            })
            setUpdateListLaboratories(!updateListLaboratories)
        } catch (error) {
            toast.error('Error al eliminar el laboratorio.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {laboratoriesData.length > 0 ? (
            <DataTable
                title="Lista de laboratorios"
                columns={columns}
                data={laboratoriesData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default LaboratoryTable