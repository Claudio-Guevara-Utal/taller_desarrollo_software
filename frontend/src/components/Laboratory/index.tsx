import { Grid, Typography } from '@mui/material'
import LaboratoryForm from './LaboratoryForm'
import LaboratoryList from './LaboratoryList'
import { useState } from 'react'
import { ILaboratoryPost, initialStateLaboratory } from '../../interfaces/laboratoryInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

const Laboratory = () => {
    const [laboratory, setLaboratory] = useState<ILaboratoryPost>(initialStateLaboratory)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [updateListLaboratories, setUpdateListLaboratories] = useState<boolean>(true)

    return (    
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Typography variant="h4">Laboratorios</Typography>
            </Grid>
            <Grid item lg={3}>
                <LaboratoryForm
                    action={action}
                    laboratory={laboratory}
                    setAction={setAction}
                    setLaboratory={setLaboratory}
                    setUpdateListLaboratories={setUpdateListLaboratories}
                    updateListLaboratories={updateListLaboratories}
                />
            </Grid>
            <Grid item lg={5}>
                <LaboratoryList
                    setAction={setAction}
                    setLaboratory={setLaboratory}
                    setUpdateListLaboratories={setUpdateListLaboratories}
                    updateListLaboratories={updateListLaboratories}
                />
            </Grid>
        </Grid>
    )
}

export default Laboratory