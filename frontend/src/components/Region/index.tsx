import { useState } from 'react'
import { IRegionPost, initialStateRegion } from '../../interfaces/regionInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

import {
    Grid,
    Typography
} from '@mui/material'

import RegionForm from './RegionForm'
import RegionList from './RegionList'

const Region = () => {
    const [region, setRegion] = useState<IRegionPost>(initialStateRegion)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [updateListRegions, setUpdateListRegions] = useState<boolean>(false)

    return (
        <Grid container spacing={3}>
            <Grid item xl={12} lg={12} xs={12}>
                <Typography variant="h4">Regiones</Typography>
            </Grid>
            <Grid item lg={3} xs={12}>
                <RegionForm
                    region={region}
                    setRegion={setRegion}
                    action={action}
                    setAction={setAction}
                    updateListRegions={updateListRegions}
                    setUpdateListRegions={setUpdateListRegions}
                />
            </Grid>
            <Grid item lg={7} xs={12}>
                <RegionList
                    updateListRegions={updateListRegions}
                    setUpdateListRegions={setUpdateListRegions}
                    setRegion={setRegion}
                    setAction={setAction}
                />
            </Grid>
        </Grid>

    )
}

export default Region