import { Dispatch, FormEvent, SetStateAction } from 'react'
import * as regionService from '../../services/regionService'
import toast from 'react-hot-toast'

import {
    Paper,
    Grid,
    TextField,
    Button
} from '@mui/material'

import { IRegionPost, initialStateRegion } from '../../interfaces/regionInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

interface Props {
    region: IRegionPost
    setRegion: Dispatch<SetStateAction<IRegionPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListRegions: boolean
    setUpdateListRegions: Dispatch<SetStateAction<boolean>>
}

const RegionForm = (props: Props) => {
    const {
        region,
        setRegion,
        action,
        setAction,
        updateListRegions,
        setUpdateListRegions
    } = props

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        try {
            if (action.type === "Crear") {
                try {
                    await regionService.createRegion(region)
                    setUpdateListRegions(!updateListRegions)
                    toast.success('Región creada con éxito.', {
                        position: 'top-center',
                        duration: 5000
                    })
                } catch (error) {
                    toast.error('Error al crear la región.', {
                        position: 'top-center',
                        duration: 5000
                    })
                }
            } else if (action.type === "Editar") {
                try {
                    await regionService.updateRegion(action.id, region)
                    setUpdateListRegions(!updateListRegions)
                    toast.success('Región actualizada con éxito.', {
                        position: 'top-center',
                        duration: 5000
                    })
                } catch (error) {
                    toast.error('Error al actualizar la región.', {
                        position: 'top-center',
                        duration: 5000
                    })
                }
            }
        } catch (error) {
            toast.error('Error al crear la región.', {
                position: 'top-center',
                duration: 5000
            })
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setRegion(initialStateRegion)
        setAction(initialStateAction)
    }

    return (
        <Paper
            sx={{
                p: 2,
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column'
            }}
        >
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            label="Región"
                            placeholder="Nombre de la región"
                            fullWidth
                            autoFocus
                            required
                            value={region.name}
                            onChange={({target}) => setRegion({...region, name: target.value})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            fullWidth
                        >
                            {action.type}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    )
}

export default RegionForm