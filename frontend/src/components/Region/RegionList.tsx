import { useEffect, useState, Dispatch, SetStateAction } from 'react'
import * as regionService from '../../services/regionService'
import toast from 'react-hot-toast'
import { IRegionGet } from '../../interfaces/regionInterface'
import RegionTable from './RegionTable'

import { IRegionPost } from '../../interfaces/regionInterface'
import { IAction } from '../../interfaces/actionInterface'

interface Props {
    updateListRegions: boolean
    setUpdateListRegions: Dispatch<SetStateAction<boolean>>
    setRegion: Dispatch<SetStateAction<IRegionPost>>
    setAction: Dispatch<SetStateAction<IAction>>
}

const RegionList = (props: Props) => {
    const { 
        updateListRegions, 
        setUpdateListRegions,
        setRegion,
        setAction
    } = props
    const [regions, setRegions] = useState<IRegionGet[]>([])

    const getRegions = async () => {
        try {
            const data = await regionService.getAllRegions()
            setRegions(data)
        } catch (error) {
            toast.error('Error al listar las regiones.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getRegions()
    }, [updateListRegions])

    return (
        <>
        {regions.length > 0 ? (
            <RegionTable
                regions={regions}
                updateListRegions={updateListRegions}
                setUpdateListRegions={setUpdateListRegions}
                setRegion={setRegion}
                setAction={setAction}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default RegionList