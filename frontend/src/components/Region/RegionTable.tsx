import { Dispatch, SetStateAction, useEffect, useState } from 'react'
import { IRegionGet } from '../../interfaces/regionInterface'
import DataTable from '../DataTable'
import { 
    Button,
} from '@mui/material'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import * as regionService from '../../services/regionService'
import toast from 'react-hot-toast'

import { IRegionPost } from '../../interfaces/regionInterface'
import { IAction } from '../../interfaces/actionInterface'

interface Props {
    regions: IRegionGet[]
    updateListRegions: boolean
    setUpdateListRegions: Dispatch<SetStateAction<boolean>>
    setRegion: Dispatch<SetStateAction<IRegionPost>>
    setAction: Dispatch<SetStateAction<IAction>>
}

type DataTableType = [string, any[]][]
const columns: string[] = ["Nombre", "Opciones"]

const RegionTable = (props: Props) => {
    const { 
        regions, 
        updateListRegions, 
        setUpdateListRegions,
        setRegion,
        setAction
    } = props
    const [regionsData, setDataRegions] = useState<DataTableType>([])

    const dataTransform = () => {
        const data: DataTableType = []
        regions.map((region) => {
            data.push([region.name, [<Button onClick={() => updateRegion(region)} color="warning"><Edit/></Button>, <Button onClick={() => deleteRegion(region._id, region.name)} color="error"><Delete/></Button>]])
        })
        setDataRegions(data)
    }
    
    useEffect(() => {
        dataTransform()
    }, [regions])

    const updateRegion = async (region: IRegionGet) => {
        setRegion({
            name: region.name
        })
        setAction({
            type: 'Editar',
            id: region._id
        })
    }

    const deleteRegion = async (id: string, regionName: string) => {
        try {
            await regionService.deleteRegion(id)
            toast.success('Región eliminada con éxito.', {
                position: 'top-center',
                duration: 5000
            })
            setUpdateListRegions(!updateListRegions)
        } catch (error) {
            toast.error('Error al eliminar la región.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }
    
    return (
        <>
        {regionsData.length > 0 ? (
            <DataTable
                title="Lista de Regiones"
                columns={columns}
                data={regionsData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default RegionTable