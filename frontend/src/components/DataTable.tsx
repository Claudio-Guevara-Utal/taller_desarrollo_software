import MUIDataTable from 'mui-datatables'

const options = {
    selectableRowsHideCheckboxes: true,
    rowsPerPageOptions: [5,10,100],
    rowsPerPage: 5
};

interface Props {
    title: string
    columns: string[]
    data: any[]
}

const DataTable = (props: Props) => {
    const { title, columns, data } = props

    return (
        <MUIDataTable
            title={title}
            columns={columns}
            data={data}
            options={options}
        />
    )
}

export default DataTable