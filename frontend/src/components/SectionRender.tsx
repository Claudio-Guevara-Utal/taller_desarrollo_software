import { useState, Dispatch, SetStateAction } from 'react'
import { Container } from '@mui/material'
import User from './User'
import Region from './Region'
import Commune from './Commune'
import BranchOffice from './BranchOffice'
import Laboratory from './Laboratory'
import Transaction from './Transaction'
import ProblemType from './ProblemType'
import Provider from './Provider'
import Order from './Order'
import SolutionType from './SolutionType'
import OrderStaff from './OrderStaff'
import LaboratoryEquipment from './LaboratoryEquipment'
import Traceability from './Traceability'
import TraceabilityFiltered from './TraceabilityFiltered'

interface Props {
    section: string
    setSection: Dispatch<SetStateAction<string>>
}

const SectionRender = (props: Props) => {
    const { section, setSection } = props
    const [transactionID, setTransactionID] = useState<string>('')

    const handleTransactionFiltered = (id: string) => {
        setTransactionID(id)
        setSection('traceability-filtered')
    } 

    return (
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            {section === 'user' && <User/>}
            {section === 'region' && <Region/>}
            {section === 'commune' && <Commune/>}
            {section === 'branch-office' && <BranchOffice/>}
            {section === 'laboratory' && <Laboratory/>}
            {section === 'transaction' && <Transaction handleTransactionFiltered={handleTransactionFiltered}/>}
            {section === 'problem-type' && <ProblemType/>}
            {section === 'provider' && <Provider/>}
            {section === 'order' && <Order/>}
            {section === 'solution-type' && <SolutionType/>}
            {section === 'orders-staff' && <OrderStaff/>}
            {section === 'laboratory-equipment' && <LaboratoryEquipment/>}
            {section === 'traceability' && <Traceability/>}
            {section === 'traceability-filtered' && <TraceabilityFiltered transactionID={transactionID} setSection={setSection}/>}
        </Container>
    )
}

export default SectionRender