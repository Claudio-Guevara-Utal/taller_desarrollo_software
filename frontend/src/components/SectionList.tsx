import { 
    Dispatch, 
    SetStateAction,
    useState
} from 'react'

import {    
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Collapse
} from '@mui/material'
import Dashboard from '@mui/icons-material/Dashboard'
import Person from '@mui/icons-material/Person'
import CorporateFare from '@mui/icons-material/CorporateFare'
import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import LabelImportant from '@mui/icons-material/LabelImportant'

interface Props {
    setSection: Dispatch<SetStateAction<string>>
    roleName: string
}

interface ISection {
    name: string
    icon: any
    text: string
}

const SectionsList = (props: Props) => {
    const { setSection, roleName } = props
    const [administrationOpen, setAdministrationOpen] = useState<boolean>(false)
    const [entityOpen, setEntityOpen] = useState<boolean>(false)
    const [transactionOpen, setTransactionOpen] = useState<boolean>(false)
    const [orderOpen, setOrderOpen] = useState<boolean>(false)

    return (
        <List>
            <ListItem button onClick={() => setSection('user')}>
                <ListItemIcon>
                    <Person/>
                </ListItemIcon>
                <ListItemText primary="Usuarios"/>
            </ListItem>
            <ListItem 
                button 
                onClick={() => setAdministrationOpen(!administrationOpen)}>
                <ListItemIcon>
                    <Dashboard/>
                </ListItemIcon>
                <ListItemText primary="Administración"/>
                { administrationOpen ? <ExpandLess/> : <ExpandMore/> }
            </ListItem>
            <Collapse in={administrationOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('region')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Regiones"/>
                        </ListItem>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('commune')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Comunas"/>
                        </ListItem>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('problem-type')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Tipos de problemas"/>
                        </ListItem>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('solution-type')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Tipos de soluciones"/>
                        </ListItem>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('laboratory-equipment')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Equipos de laboratorio"/>
                        </ListItem>
                    </List>
                </Collapse>
                <ListItem button onClick={() => setEntityOpen(!entityOpen)}>
                    <ListItemIcon>
                        <CorporateFare/>
                    </ListItemIcon>
                    <ListItemText primary="Entidades"/>
                    { entityOpen ? <ExpandLess/> : <ExpandMore/> }
                </ListItem>
                <Collapse in={entityOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('branch-office')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Sucursales"/>
                        </ListItem>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('laboratory')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Laboratorios"/>
                        </ListItem>
                        <ListItem
                            button
                            sx={{ paddingLeft: 8 }}
                            onClick={() => setSection('provider')}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Proveedores"/>
                        </ListItem>
                    </List>
                </Collapse>
                <ListItem 
                    button 
                    onClick={() => setTransactionOpen(!transactionOpen)}>
                    <ListItemIcon>
                        <Dashboard/>
                    </ListItemIcon>
                    <ListItemText primary="Nombre"/>
                    { transactionOpen ? <ExpandLess/> : <ExpandMore/> }
                </ListItem>
                <Collapse in={transactionOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem 
                            button 
                            onClick={() => setSection('transaction')}
                            sx={{ paddingLeft: 8 }}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Transacciones"/>
                        </ListItem>
                    </List>
                    <List component="div" disablePadding>
                        <ListItem 
                            button 
                            onClick={() => setSection('order')}
                            sx={{ paddingLeft: 8 }}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Pedidos"/>
                        </ListItem>
                    </List>
                    <List component="div" disablePadding>
                        <ListItem 
                            button 
                            onClick={() => setSection('orders-staff')}
                            sx={{ paddingLeft: 8 }}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Pendientes"/>
                        </ListItem>
                    </List>
                    <List component="div" disablePadding>
                        <ListItem 
                            button 
                            onClick={() => setSection('traceability')}
                            sx={{ paddingLeft: 8 }}
                        >
                            <ListItemIcon>
                                <LabelImportant/>
                            </ListItemIcon>
                            <ListItemText primary="Trazabilidad"/>
                        </ListItem>
                    </List>
                </Collapse>
        </List>
    )
}

export default SectionsList