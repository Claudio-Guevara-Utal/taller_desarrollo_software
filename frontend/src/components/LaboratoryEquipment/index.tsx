import {
    Grid,
    Typography
} from '@mui/material'
import LaboratoryEquipmentForm from './LaboratoryEquipmentForm'
import LaboratoryEquipmentList from './LaboratoryEquipmentList'
import { useState } from 'react'
import { ILaboratoryEquipmentPost, initialStateLaboratoryEquipment } from '../../interfaces/laboratoryEquipmentInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

const LaboratoryEquipment = () => {
    const [laboratoryEquipment, setLaboratoryEquipment] = useState<ILaboratoryEquipmentPost>(initialStateLaboratoryEquipment)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [updateListLaboratoryEquipments, setUpdateListLaboratoryEquipments] = useState<boolean>(true)

    return (
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Typography variant="h4">Equipos de laboratorio</Typography>
            </Grid>
            <Grid item lg={3}>
                <LaboratoryEquipmentForm
                    action={action}
                    laboratoryEquipment={laboratoryEquipment}
                    setAction={setAction}
                    setLaboratoryEquipment={setLaboratoryEquipment}
                    setUpdateListLaboratoryEquipments={setUpdateListLaboratoryEquipments}
                    updateListLaboratoryEquipments={updateListLaboratoryEquipments}
                />
            </Grid>
            <Grid item lg={6}> 
                <LaboratoryEquipmentList
                    setAction={setAction}
                    setLaboratoryEquipment={setLaboratoryEquipment}
                    setUpdateListLaboratoryEquipments={setUpdateListLaboratoryEquipments}
                    updateListLaboratoryEquipments={updateListLaboratoryEquipments}
                />
            </Grid>
        </Grid>
    )
}

export default LaboratoryEquipment