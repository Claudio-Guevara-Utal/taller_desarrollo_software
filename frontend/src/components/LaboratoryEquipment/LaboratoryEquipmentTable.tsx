import DataTable from '../DataTable'
import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { ILaboratoryEquipmentGet, ILaboratoryEquipmentPost } from '../../interfaces/laboratoryEquipmentInterface'
import { IAction } from '../../interfaces/actionInterface'
import * as laboratoryEquipmentService from '../../services/laboratoryEquipmentService'
import { Button } from '@mui/material'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import toast from 'react-hot-toast'

type DataTableType = [string, string, any[]][]
const columns: string[] = ["Nombre", "Función", "Opciones"]

interface Props {
    laboratoryEquipments: ILaboratoryEquipmentGet[]
    setLaboratoryEquipment: Dispatch<SetStateAction<ILaboratoryEquipmentPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListLaboratoryEquipments: boolean
    setUpdateListLaboratoryEquipments: Dispatch<SetStateAction<boolean>>
}

const LaboratoryEquipmentTable = (props: Props) => {
    const {
        laboratoryEquipments,
        setLaboratoryEquipment,
        setAction,
        updateListLaboratoryEquipments,
        setUpdateListLaboratoryEquipments
    } = props
    const [laboratoryEquipmentsData, setLaboratoryEquipmentsData] = useState<DataTableType>([])

    const dataTransform = async () => {
        let data: DataTableType = []
        laboratoryEquipments.map((laboratoryEquipment) => {
            data.push([
                laboratoryEquipment.name,
                laboratoryEquipment.function,
                [<Button onClick={() => updateLaboratoryEquipment(laboratoryEquipment)} color="warning"><Edit/></Button>, <Button onClick={() => deleteLaboratoryEquipment(laboratoryEquipment._id)} color="error"><Delete/></Button>]
            ])
        })
        setLaboratoryEquipmentsData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [laboratoryEquipments])

    const updateLaboratoryEquipment = (laboratoryEquipment: ILaboratoryEquipmentGet) => {
        setLaboratoryEquipment({
            name: laboratoryEquipment.name,
            function: laboratoryEquipment.function
        })
        setAction({
            type: 'Editar',
            id: laboratoryEquipment._id
        })
    }

    const deleteLaboratoryEquipment = async (id: string) => {
        try {
            await laboratoryEquipmentService.deleteLaboratoryEquipment(id)
            setUpdateListLaboratoryEquipments(!updateListLaboratoryEquipments)
            toast.success('Equipo eliminado con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al eliminar el equipo.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {laboratoryEquipmentsData.length > 0 ? (
            <DataTable
                title="Lista de equipos de laboratorio"
                columns={columns}
                data={laboratoryEquipmentsData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default LaboratoryEquipmentTable