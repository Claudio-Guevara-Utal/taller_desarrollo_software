import {
    Paper,
    Grid,
    TextField,
    Button
} from '@mui/material'
import { ILaboratoryEquipmentPost, initialStateLaboratoryEquipment } from '../../interfaces/laboratoryEquipmentInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import { Dispatch, SetStateAction, FormEvent } from 'react'
import toast from 'react-hot-toast'
import * as laboratoryEquipmentService from '../../services/laboratoryEquipmentService'

interface Props {
    laboratoryEquipment: ILaboratoryEquipmentPost
    setLaboratoryEquipment: Dispatch<SetStateAction<ILaboratoryEquipmentPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListLaboratoryEquipments: boolean
    setUpdateListLaboratoryEquipments: Dispatch<SetStateAction<boolean>>
}

const LaboratoryEquipmentForm = (props: Props) => {
    const {
        laboratoryEquipment,
        setLaboratoryEquipment,
        action,
        setAction,
        updateListLaboratoryEquipments,
        setUpdateListLaboratoryEquipments
    } = props

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await laboratoryEquipmentService.createLaboratoryEquipment(laboratoryEquipment)
                setUpdateListLaboratoryEquipments(!updateListLaboratoryEquipments)
                toast.success('Equipo creado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al crear el equipo.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        } else if (action.type === 'Editar') {
            try {
                await laboratoryEquipmentService.updateLaboratoryEquipment(action.id, laboratoryEquipment)
                setUpdateListLaboratoryEquipments(!updateListLaboratoryEquipments)
                toast.success('Equipo actualizado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al actualizar el equipo.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setLaboratoryEquipment(initialStateLaboratoryEquipment)
        setAction(initialStateAction)
    }

    return (
        <Paper
            sx={{
                p: 2,
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column'
            }}
        >
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            label="Nombre"
                            placeholder="Nombre"
                            fullWidth
                            autoFocus
                            required
                            value={laboratoryEquipment.name}
                            onChange={({target}) => setLaboratoryEquipment({...laboratoryEquipment, name: target.value})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Función"
                            placeholder="Función del equipo"
                            fullWidth
                            required
                            value={laboratoryEquipment.function}
                            onChange={({target}) => setLaboratoryEquipment({...laboratoryEquipment, function: target.value})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            color="primary"
                            variant="contained"
                            fullWidth
                        >
                            {action.type}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>   
                    </Grid>
                </Grid>
            </form>
        </Paper>
    )
}

export default LaboratoryEquipmentForm