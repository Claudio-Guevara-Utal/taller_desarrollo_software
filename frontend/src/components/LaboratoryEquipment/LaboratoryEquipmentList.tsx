import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { ILaboratoryEquipmentGet, ILaboratoryEquipmentPost } from '../../interfaces/laboratoryEquipmentInterface'
import { IAction } from '../../interfaces/actionInterface'
import * as laboratoryEquipmentService from '../../services/laboratoryEquipmentService'
import toast from 'react-hot-toast'
import LaboratoryEquipmentTable from './LaboratoryEquipmentTable'

interface Props {
    setLaboratoryEquipment: Dispatch<SetStateAction<ILaboratoryEquipmentPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListLaboratoryEquipments: boolean
    setUpdateListLaboratoryEquipments: Dispatch<SetStateAction<boolean>>
}

const LaboratoryEquipmentList = (props: Props) => {
    const {
        setLaboratoryEquipment,
        setAction,
        updateListLaboratoryEquipments,
        setUpdateListLaboratoryEquipments
    } = props
    const [laboratoryEquipments, setLaboratoryEquipments] = useState<ILaboratoryEquipmentGet[]>([])

    const getLaboratoryEquipments = async () => {
        try {
            const data = await laboratoryEquipmentService.getAllLaboratoryEquipments()
            setLaboratoryEquipments(data)
        } catch (error) {
            toast.error('Error al listar los equipos de laboratorio.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getLaboratoryEquipments()
    }, [updateListLaboratoryEquipments])

    return (
        <>
        {laboratoryEquipments.length > 0 ? (
            <LaboratoryEquipmentTable
                laboratoryEquipments={laboratoryEquipments}
                setAction={setAction}
                setLaboratoryEquipment={setLaboratoryEquipment}
                setUpdateListLaboratoryEquipments={setUpdateListLaboratoryEquipments}
                updateListLaboratoryEquipments={updateListLaboratoryEquipments}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default LaboratoryEquipmentList