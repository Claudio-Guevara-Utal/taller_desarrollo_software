import {
    Paper,
    Grid,
    TextField,
    Button
} from '@mui/material'
import { Dispatch, FormEvent, SetStateAction } from 'react'
import { ISolutionTypePost, initialStateSolutionType } from '../../interfaces/solutionTypeInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import * as solutionTypeService from '../../services/solutionTypeService'
import toast from 'react-hot-toast'

interface Props {
    solutionType: ISolutionTypePost
    setSolutionType: Dispatch<SetStateAction<ISolutionTypePost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListSolutionTypes: boolean
    setUpdateListSolutionTypes: Dispatch<SetStateAction<boolean>>
}

const SolutionTypeForm = (props: Props) => {
    const {
        solutionType,
        setSolutionType,
        action,
        setAction,
        updateListSolutionTypes,
        setUpdateListSolutionTypes
    } = props

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await solutionTypeService.createSolutionType(solutionType)
                setUpdateListSolutionTypes(!updateListSolutionTypes)
                toast.success('Tipo de solución creada con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al crear el tipo de solución.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        } else if (action.type === 'Editar') {
            try {
                await solutionTypeService.updateSolutionType(action.id, solutionType)
                setUpdateListSolutionTypes(!updateListSolutionTypes)
                toast.success('Tipo de solución actualizada con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al actualizar el tipo de solución.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setSolutionType(initialStateSolutionType)
        setAction(initialStateAction)
    }

    return (
        <Paper
            sx={{
                p: 2,
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column'
            }}
        >
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            label="Tipo de Respuesta"
                            placeholder="Nombre"
                            autoFocus
                            required
                            fullWidth
                            value={solutionType.name}
                            onChange={({target}) => setSolutionType({...solutionType, name: target.value})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            color="primary"
                            variant="contained"
                            fullWidth
                        >
                            {action.type}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    )
}

export default SolutionTypeForm