import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import DataTable from '../DataTable'
import { ISolutionTypeGet, ISolutionTypePost } from '../../interfaces/solutionTypeInterface'
import { IAction } from '../../interfaces/actionInterface'
import { Button } from '@mui/material'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import * as solutionTypeService from '../../services/solutionTypeService'
import toast from 'react-hot-toast'

const columns: string[] = ["Nombre", "Opciones"]
type DataTableType = [string, any[]][]

interface Props {
    solutionTypes: ISolutionTypeGet[]
    setSolutionType: Dispatch<SetStateAction<ISolutionTypePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListSolutionTypes: boolean
    setUpdateListSolutionTypes: Dispatch<SetStateAction<boolean>>
}

const AnswerTypeTable = (props: Props) => {
    const {
        solutionTypes,
        setSolutionType,
        setAction,
        updateListSolutionTypes,
        setUpdateListSolutionTypes
    } = props
    const [solutionTypesData, setSolutionTypesData] = useState<DataTableType>([])

    const dataTransform = () => {
        let data: DataTableType = []
        solutionTypes.map((solutionType) => {
            data.push([
                solutionType.name, 
                [<Button onClick={() => updateSolutionType(solutionType)} color="warning"><Edit/></Button>, <Button onClick={() => deleteSolutionType(solutionType._id)} color="error"><Delete/></Button>]
            ])
        })
        setSolutionTypesData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [solutionTypes])

    const updateSolutionType = (solutionType: ISolutionTypeGet) => {
        setSolutionType({
            name: solutionType.name
        })
        setAction({
            type: 'Editar',
            id: solutionType._id
        })
    }

    const deleteSolutionType = async (id: string) => {
        try {
            await solutionTypeService.deleteSolutionType(id)
            setUpdateListSolutionTypes(!updateListSolutionTypes)
            toast.success('Tipo de respuesta eliminado con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al eliminar el tipo de solución.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {solutionTypes.length > 0 ? (
            <DataTable
                title="Lista de los tipos de respuestas"
                columns={columns}
                data={solutionTypesData}
            />
        ) : (
            'cargando..'
        )}
        </>
    )
}

export default AnswerTypeTable