import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { ISolutionTypeGet, ISolutionTypePost } from '../../interfaces/solutionTypeInterface'
import { IAction } from '../../interfaces/actionInterface'
import SolutionTypeTable from './SolutionTypeTable'
import { getAllSolutionTypes } from '../../services/solutionTypeService'
import toast from 'react-hot-toast'

interface Props {
    setSolutionType: Dispatch<SetStateAction<ISolutionTypePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListSolutionTypes: boolean
    setUpdateListSolutionTypes: Dispatch<SetStateAction<boolean>>
}

const AnswerTypeList = (props: Props) => {
    const {
        setSolutionType,
        setAction,
        updateListSolutionTypes,
        setUpdateListSolutionTypes
    } = props
    const [solutionTypes, setSolutionTypes] = useState<ISolutionTypeGet[]>([])

    const getSolutionTypes = async () => {
        try {
            const data = await getAllSolutionTypes()
            setSolutionTypes(data)
        } catch (error) {
            toast.error('Error al listar los tipos de soluciones.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getSolutionTypes()
    }, [updateListSolutionTypes])

    return (
        <>
        {solutionTypes.length > 0 ? (
            <SolutionTypeTable
                solutionTypes={solutionTypes}
                setAction={setAction}
                setSolutionType={setSolutionType}
                updateListSolutionTypes={updateListSolutionTypes}
                setUpdateListSolutionTypes={setUpdateListSolutionTypes}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default AnswerTypeList