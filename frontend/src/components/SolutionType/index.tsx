import { Grid, Typography } from '@mui/material'
import { useState } from 'react'
import { ISolutionTypePost, initialStateSolutionType } from '../../interfaces/solutionTypeInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

import SolutionTypeForm from './SolutionTypeForm'
import SolutionTypeList from './SolutionTypeList'

const SolutionType = () => {
    const [solutionType, setSolutionType] = useState<ISolutionTypePost>(initialStateSolutionType)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [updateListSolutionTypes, setUpdateListSolutionTypes] = useState<boolean>(true) 

    return (
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Typography variant="h4">Tipos de soluciones</Typography>
            </Grid>
            <Grid item lg={3}>
                <SolutionTypeForm
                    action={action}
                    solutionType={solutionType}
                    setAction={setAction}
                    setSolutionType={setSolutionType}
                    setUpdateListSolutionTypes={setUpdateListSolutionTypes}
                    updateListSolutionTypes={updateListSolutionTypes}
                />
            </Grid>
            <Grid item lg={5}>
                <SolutionTypeList
                    setAction={setAction}
                    setSolutionType={setSolutionType}
                    setUpdateListSolutionTypes={setUpdateListSolutionTypes}
                    updateListSolutionTypes={updateListSolutionTypes}
                />
            </Grid>
        </Grid>
    )
}

export default SolutionType