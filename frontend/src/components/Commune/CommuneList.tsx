import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import * as communeService from '../../services/communeService'
import { ICommuneGet, ICommunePost } from '../../interfaces/communeInterface'
import { IAction } from '../../interfaces/actionInterface'
import CommuneTable from './CommuneTable'
import toast from 'react-hot-toast'

interface Props {
    updateListCommunes: boolean
    setCommune: Dispatch<SetStateAction<ICommunePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    setUpdateListCommunes: Dispatch<SetStateAction<boolean>>
}

const CommuneList = (props: Props) => {
    const {
        updateListCommunes,
        setCommune,
        setAction,
        setUpdateListCommunes
    } = props
    const [communes, setCommunes] = useState<ICommuneGet[]>([])

    const getCommunes = async () => {
        try {
            const data = await communeService.getAllCommunes()
            setCommunes(data)
        } catch (error) {
            toast.error('Error al listar las comunas.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getCommunes()
    }, [updateListCommunes])

    return (
        <>
        {communes.length > 0 ? (
            <CommuneTable
                communes={communes}
                setAction={setAction}
                setCommune={setCommune}
                setUpdateListCommunes={setUpdateListCommunes}
                updateListCommunes={updateListCommunes}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default CommuneList 