import { useState } from 'react'
import {
    Grid,
    Typography
} from '@mui/material'
import { ICommunePost, initialStateCommune } from '../../interfaces/communeInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'

import CommuneForm from './CommuneForm'
import CommuneList from './CommuneList'

const Commune = () => {
    const [commune, setCommune] = useState<ICommunePost>(initialStateCommune)
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [updateListCommunes, setUpdateListCommunes] = useState<boolean>(true)

    return (
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Typography variant="h4">Comunas</Typography>
            </Grid>
            <Grid item lg={3}>
                <CommuneForm
                    commune={commune}
                    setCommune={setCommune}
                    action={action}
                    setAction={setAction}
                    updateListCommunes={updateListCommunes}
                    setUpdateListCommunes={setUpdateListCommunes}
                />
            </Grid>
            <Grid item lg={5}>
                <CommuneList
                    updateListCommunes={updateListCommunes}
                    setAction={setAction}
                    setCommune={setCommune}
                    setUpdateListCommunes={setUpdateListCommunes}
                />
            </Grid> 
        </Grid>
    )
}

export default Commune