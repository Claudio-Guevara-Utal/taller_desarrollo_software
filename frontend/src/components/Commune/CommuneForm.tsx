import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import { IRegionGet } from '../../interfaces/regionInterface'
import {
    Paper,
    Grid,
    TextField,
    Autocomplete,
    Button
} from '@mui/material'
import * as regionService from '../../services/regionService'
import { ICommunePost, initialStateCommune } from '../../interfaces/communeInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import * as communeService from '../../services/communeService'
import toast from 'react-hot-toast'

interface Props {
    commune: ICommunePost
    setCommune: Dispatch<SetStateAction<ICommunePost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListCommunes: boolean
    setUpdateListCommunes: Dispatch<SetStateAction<boolean>>
}

const CommuneForm = (props: Props) => {
    const {
        commune,
        setCommune,
        action,
        setAction,
        updateListCommunes,
        setUpdateListCommunes
    } = props
    const [regions, setRegions] = useState<IRegionGet[]>([])

    const getRegions = async () => {
        try {
            const data = await regionService.getAllRegions()
            setRegions(data)
        } catch (error) {
            toast.error('Error al listar las regiones.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getRegions()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await communeService.createCommune(commune)
                setUpdateListCommunes(!updateListCommunes)
                toast.success('Comuna creada con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al crear la comuna.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        } else if (action.type === 'Editar') {
            try {   
                await communeService.updateCommune(action.id, commune)
                setUpdateListCommunes(!updateListCommunes)
                toast.success('Comuna actualizada con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error) {
                toast.error('Error al actualizar la comuna.', {
                    position: 'top-center',
                    duration: 5000
                })
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setCommune(initialStateCommune)
        setAction(initialStateAction)
    }

    return (
        <>
        {regions.length > 0 ? (
            <Paper
                sx={{
                    p: 2,
                    display: 'flex',
                    justifyContent: 'center',
                    flexDirection: 'column'
                }}
            >
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <TextField
                                label="Comuna"
                                placeholder="Nombre de la comuna"
                                autoFocus
                                fullWidth
                                value={commune.name}
                                required
                                onChange={({target}) => setCommune({...commune, name: target.value})}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Autocomplete
                                disablePortal
                                options={regions.map((region) => region.name)}
                                renderInput={(params) => <TextField {...params} required label="Región"/>}
                                value={commune.region}
                                onChange={(event, region) => setCommune({...commune, region: region})}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                fullWidth
                            >
                                {action.type}
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                variant="contained"
                                color="error"
                                fullWidth
                                onClick={onClickCancelar}
                            >
                                Cancelar
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default CommuneForm