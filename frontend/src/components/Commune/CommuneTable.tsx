import { useEffect, useState, Dispatch, SetStateAction } from 'react'
import { ICommuneGet, ICommunePost } from '../../interfaces/communeInterface'
import { Button } from '@mui/material'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import HorizontalRule from '@mui/icons-material/HorizontalRule'
import DataTable from '../DataTable'
import { IAction } from '../../interfaces/actionInterface'
import * as communeService from '../../services/communeService'
import toast from 'react-hot-toast'

interface Props {
    communes: ICommuneGet[]
    setCommune: Dispatch<SetStateAction<ICommunePost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListCommunes: boolean
    setUpdateListCommunes: Dispatch<SetStateAction<boolean>>
}

type DataTableType = [string, string | any, any[]][]
const columns: string[] = ["Nombre", "Región", "Opciones"]

const CommuneTable = (props: Props) => {
    const [communesData, setCommunesData] = useState<DataTableType>([])
    const { 
        communes,
        setCommune,
        setAction,
        updateListCommunes,
        setUpdateListCommunes
    } = props

    const dataTransform = () => {
        let data: DataTableType = []
        communes.map((commune) => {
            data.push([
                commune.name, 
                commune.region ? commune.region.name : <HorizontalRule/>, 
                [<Button onClick={() => updateCommune(commune)} color="warning"><Edit/></Button>, <Button onClick={() => deleteCommune(commune._id)} color="error"><Delete/></Button>]
            ])
            setCommunesData(data)
        })
    }
    
    useEffect(() => {
        dataTransform()
    }, [communes])

    const updateCommune = async (commune: ICommuneGet) => {
        setCommune({
            name: commune.name,
            region: commune.region ? commune.region.name : ''
        })
        setAction({
            type: 'Editar',
            id: commune._id
        })
    }

    const deleteCommune = async (id: string) => {
        try {
            await communeService.deleteCommune(id)
            setUpdateListCommunes(!updateListCommunes)
            toast.success('Comuna eliminada con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al eliminar la comuna.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {communesData.length > 0 ? (
            <DataTable
                title="Lista de comunas"
                columns={columns}
                data={communesData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default CommuneTable