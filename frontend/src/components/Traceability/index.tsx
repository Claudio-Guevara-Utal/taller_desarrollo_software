import { Grid, Typography } from '@mui/material'
import { useState, useEffect } from 'react'
import { IProblemGet } from '../../interfaces/problemInterface'
import * as problemService from '../../services/problemService' 
import toast from 'react-hot-toast'
import TraceabilityTable from './TraceabilityTable'

const Traceability = () => {
    const [problems, setProblems] = useState<IProblemGet[]>([])

    const getProblems = async () => {
        try {
            const data = await problemService.getAllProblems()
            setProblems(data)
        } catch (error) {
            toast.error('Errora al listar los problemas.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getProblems()
    }, [])

    return (
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <Typography variant="h4">Trazabilidad</Typography>
            </Grid>
            <Grid item lg={12}>
                <TraceabilityTable
                    problems={problems}
                />
            </Grid>
        </Grid>
    )
}

export default Traceability