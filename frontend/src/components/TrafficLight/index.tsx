import './index.css'

interface Props {
    colour: string | undefined
}

const TrafficLigt = (props: Props) => {
    const { colour } = props

    return (
        <div className="traffic-light">
            <div className="container">
                <div className={colour === 'red' ? 'circle red' : 'circle' }></div>
                <div className={colour === 'yellow' ? 'circle yellow' : 'circle' }></div>
                <div className={colour === 'green' ? 'circle green' : 'circle' }></div>
            </div>
        </div>
    )   
}

export default TrafficLigt