import { Dispatch, SetStateAction } from 'react'
import { Grid } from '@mui/material'
import { styled, Box } from '@mui/system'
import { ModalUnstyled } from '@mui/core'
import OrderForm from './OrderForm'

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const style = {
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
    px: 4,
    pb: 3,
};

interface Props {
    open: boolean
    handleClose: () => void
    orderID: string
    problemID: string
    updateListOrders: boolean
    setUpdateListOrders: Dispatch<SetStateAction<boolean>>
}

const OrderModal = (props: Props) => {
    const {
        open,
        handleClose,
        orderID,
        problemID,
        updateListOrders,
        setUpdateListOrders
    } = props

    return (
        <Grid container justifyContent="flex-end" direction="row">
            <StyledModal
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <OrderForm
                        handleClose={handleClose}
                        orderID={orderID}
                        problemID={problemID}
                        updateListOrders={updateListOrders}
                        setUpdateListOrders={setUpdateListOrders}
                    />
                </Box>
            </StyledModal>
        </Grid>
    )
}

export default OrderModal