import { useState, useEffect, FormEvent, Dispatch, SetStateAction } from 'react'
import { IProviderStaffEmailGet } from '../../interfaces/providerStaffInterface'
import {  } from '../../interfaces/orderInterface'
import * as providerStaffService from '../../services/providerStaffService'
import * as orderService from '../../services/orderService'
import toast from 'react-hot-toast'
import {
    Grid,
    FormControl,
    InputLabel,
    Select,
    OutlinedInput,
    Box,
    Chip,
    MenuItem,
    Button,
    SelectChangeEvent
} from '@mui/material'
import { Theme, useTheme } from '@mui/material/styles';
import Close from '@mui/icons-material/Close'

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(email: string, providerEmail: readonly string[], theme: Theme) {
    return {
      fontWeight:
        providerEmail.indexOf(email) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}

interface Props {
    handleClose: () => void
    orderID: string
    problemID: string
    updateListOrders: boolean
    setUpdateListOrders: Dispatch<SetStateAction<boolean>>
}

const OrderForm = (props: Props) => {
    const {
        handleClose,
        orderID,
        problemID,
        updateListOrders,
        setUpdateListOrders
    } = props
    const theme = useTheme();
    const [providersStaff, setProvidersStaff] = useState<IProviderStaffEmailGet[]>([])
    const [providerEmail, setProviderEmail] = useState<string[]>([])

    const handleChange = (e: SelectChangeEvent<typeof providerEmail>) => {
        const {
            target: { value }
        } = e
        setProviderEmail(
            typeof value === 'string' ? value.split(',') : value
        )
    }

    const getProvidersStaff = async () => {
        try {
            const data = await providerStaffService.getAllProvidersStaffEmail()
            setProvidersStaff(data)
        } catch (error) {
            toast.error('Error al listar los personales técnicos.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getProvidersStaff()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const date = new Date().toISOString()
        try {
            await orderService.assignProvidersStaff(orderID, problemID, providerEmail, date)
            setUpdateListOrders(!updateListOrders)
            toast.success('Técnicos asignados con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al asignar los técnicos.', {
                position: 'top-center',
                duration: 5000
            })
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setProviderEmail([])
    }

    return (
        <>
        {providersStaff.length > 0 ? (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid container justifyContent="flex-end" direction="row">
                        <Button color="primary"><Close onClick={handleClose}/></Button>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl sx={{ m: 1, width: 300 }}>
                            <InputLabel id="multiple-tecnico-label">Técnico</InputLabel>
                            <Select
                                labelId="multiple-tecnico-label"
                                multiple
                                value={providerEmail}
                                onChange={handleChange}
                                input={<OutlinedInput label="Técnico" />}
                                renderValue={(selected) => (
                                    <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                        {selected.map((value) => (
                                            <Chip key={value} label={value}/>
                                        ))}
                                    </Box>
                                )}
                                MenuProps={MenuProps}
                            >   
                                {providersStaff.map((staff) => (
                                    <MenuItem
                                        key={staff.email}
                                        value={staff.email}
                                        style={getStyles(staff.email, providerEmail, theme)}
                                    >
                                        {staff.email}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>  
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            color="primary"
                            variant="contained"
                            fullWidth
                        >
                            Asignar
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default OrderForm