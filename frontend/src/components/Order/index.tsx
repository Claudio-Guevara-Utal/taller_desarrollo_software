import { useState, useEffect } from 'react'
import { IOrderGet } from '../../interfaces/orderInterface'
import * as orderService from '../../services/orderService'
import toast from 'react-hot-toast'
import { Grid, Typography } from '@mui/material'
import OrderTable from './OrderTable'
import OrderModal from './OrderModal'

const Order = () => {
    const [orders, setOrders] = useState<IOrderGet[]>([])
    const [open, setOpen] = useState<boolean>(false)
    const [orderID, setOrderID] = useState<string>('')
    const [problemID, setProblemID] = useState<string>('')
    const [updateListOrders, setUpdateListOrders] = useState<boolean>(true)

    const handleOpen = (orderID: string, problemID: string): void => {
        setOpen(true)
        setOrderID(orderID)
        setProblemID(problemID)
    }
    const handleClose = (): void => setOpen(false)
 
    const getOrders = async () => {
        try {
            const data = await orderService.getAllOrders()
            setOrders(data)
        } catch (error) {
            toast.error('Error al listar los pedidos.', {
                position: 'top-center',
                duration: 5000  
            })
        }
    }

    useEffect(() => {
        getOrders()
    }, [updateListOrders])

    return (
        <>
        {orders.length > 0 ? (
            <Grid container spacing={3}>
                <Grid item lg={12}>
                    <Typography variant="h4">Pedidos</Typography>
                </Grid>
                <Grid item lg={12}>
                    <OrderTable
                        orders={orders}
                        handleOpen={handleOpen}
                    />
                </Grid>
                <Grid item lg={6}>
                    <OrderModal
                        open={open}
                        handleClose={handleClose}
                        orderID={orderID}
                        problemID={problemID}
                        updateListOrders={updateListOrders}
                        setUpdateListOrders={setUpdateListOrders}
                    />
                </Grid>
            </Grid>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default Order