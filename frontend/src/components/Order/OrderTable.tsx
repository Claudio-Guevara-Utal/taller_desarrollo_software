import { IOrderGet } from '../../interfaces/orderInterface'
import { useState, useEffect } from 'react'
import DataTable from '../DataTable'
import { Button } from '@mui/material'
import HorizontalRule from '@mui/icons-material/HorizontalRule'
import AssignmentInd from '@mui/icons-material/AssignmentInd'
import QuestionMark from '@mui/icons-material/QuestionMark'
import TrafficLight from '../TrafficLight'

interface Props {
    orders: IOrderGet[]
    handleOpen: (orderID: string, problemID: string) => void
}

const columns: string[] = ["Proveedor", "Fecha del Pedido", "Equipo", "Sucursal", "Problema", "Fecha del Problema", "Técnicos", "Fecha de asignación", "Estado", "Opciones"]
type DataTableType = any[]

const OrderTable = (props: Props) => {
    const { orders, handleOpen } = props
    const [ordersData, setOrdersData] = useState<any[]>([])

    const dateFormat = (date: string) => {
        const newDate = date.split("T")[0]
        return newDate
    }

    const providersFormat = (providers: { email: string }[]) => {
        let cadena = ''
        providers.map((provider, key) => {
            if (key + 1 < providers.length) {
                cadena = cadena + provider.email + ' - '
            } else {
                cadena = cadena + provider.email
            }
        })
        return cadena
    }

    const dataTransform = async () => {
        let data: DataTableType = []
        orders.map((order) => {
            let colour 
            if (!order.problem) {
                colour = ''
            } else if (order.problem.solved) {
                colour = 'green'
            } else if (order.problem.solved === false && order.providersStaff.length > 0) {
                colour = 'yellow'
            } else if (order.problem.solved === false && order.providersStaff.length === 0) {
                colour = 'red'
            }

            data.push([
                order.transaction ? order.transaction.provider ? order.transaction.provider.name : <HorizontalRule/> : <HorizontalRule/>,
                dateFormat(order.createdAt),
                order.transaction ? order.transaction.laboratoryEquipment ? order.transaction.laboratoryEquipment.name : <HorizontalRule/> : <HorizontalRule/>,
                order.transaction ? order.transaction.laboratory ? order.transaction.laboratory.branchOffice ? order.transaction.laboratory.branchOffice.name : <HorizontalRule/> : <HorizontalRule/> : <HorizontalRule/>,
                order.problem ? order.problem.problemType ? order.problem.problemType.name : <HorizontalRule/> : <HorizontalRule/>,
                order.problem ? dateFormat(order.problem.problemDate) : <HorizontalRule/>,
                order.providersStaff.length > 0 ? providersFormat(order.providersStaff) : <QuestionMark/>,
                order.staffAssignmentDate ? dateFormat(order.staffAssignmentDate) : <QuestionMark/>,
                <TrafficLight colour={colour}/>,
                [<Button onClick={() => handleOpen(order._id, order.problem._id)} color="info"><AssignmentInd/></Button>]
            ])  
        })
        setOrdersData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [orders])

    return (
        <>
        {ordersData.length > 0 ? (
            <DataTable
                title="Lista de pedidos"
                columns={columns}
                data={ordersData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default OrderTable