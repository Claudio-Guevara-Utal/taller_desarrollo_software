import { useState, useEffect, Dispatch, SetStateAction } from "react"
import { ITransactionGet, ITransactionPost } from '../../interfaces/transactionInterface'
import { IAction } from '../../interfaces/actionInterface'
import TransactionTable from './TransactionTable'
import * as transactionService from '../../services/transactionService'
import toast from "react-hot-toast"

interface Props {
    setTransaction: Dispatch<SetStateAction<ITransactionPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListTransactions: boolean
    setUpdateListTransactions: Dispatch<SetStateAction<boolean>>
    handleOpen: () => void
    handleProblemModalOpen: (id: string) => void
    handleTransactionFiltered: (id: string) => void
}

const TransactionList = (props: Props) => {
    const {
        setTransaction,
        setAction,
        updateListTransactions,
        setUpdateListTransactions,
        handleOpen,
        handleProblemModalOpen,
        handleTransactionFiltered
    } = props
    const [transactions, setTransactions] = useState<ITransactionGet[]>([])

    const getTransactions = async () => {
        try {
            const data = await transactionService.getAllTransactions()
            setTransactions(data)
        } catch (error) {
            toast.error('Error al listar las transacciones.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getTransactions()
    }, [updateListTransactions])

    return (
        <>
        {transactions.length > 0 ? (
            <TransactionTable
                transactions={transactions}
                handleOpen={handleOpen}
                setAction={setAction}
                setTransaction={setTransaction}
                setUpdateListTransactions={setUpdateListTransactions}
                updateListTransactions={updateListTransactions}
                handleProblemModalOpen={handleProblemModalOpen}
                handleTransactionFiltered={handleTransactionFiltered}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default TransactionList