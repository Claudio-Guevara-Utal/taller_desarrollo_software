import { Dispatch, SetStateAction } from 'react'
import { Grid, Button } from '@mui/material'
import { styled, Box } from '@mui/system'
import { ModalUnstyled } from '@mui/core'
import TransactionForm from './TransactionForm'
import { ITransactionPost } from '../../interfaces/transactionInterface'
import { IAction } from '../../interfaces/actionInterface'
import Add from '@mui/icons-material/Add'

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const style = {
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
    px: 4,
    pb: 3,
};

interface Props {
    open: boolean
    handleOpen: () => void
    handleClose: () => void
    transaction: ITransactionPost
    setTransaction: Dispatch<SetStateAction<ITransactionPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListTransactions: boolean
    setUpdateListTransactions: Dispatch<SetStateAction<boolean>>
}

const TransactionModal = (props: Props) => {
    const {
        open,
        handleOpen,
        handleClose,
        transaction,
        setTransaction,
        action,
        setAction,
        updateListTransactions,
        setUpdateListTransactions
    } = props

    return (
        <Grid container justifyContent="flex-end" direction="row">
            <Button
                type="button"
                variant="contained"
                color="info"
                onClick={handleOpen}
            >
                <Add/> <span style={{ marginLeft: 5 }}>Añadir Transacción</span>
            </Button>
            <StyledModal
                open={open}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <TransactionForm
                        handleClose={handleClose}
                        transaction={transaction}
                        setTransaction={setTransaction}
                        action={action}
                        setAction={setAction}
                        updateListTransactions={updateListTransactions}
                        setUpdateListTransactions={setUpdateListTransactions}
                    />
                </Box>
            </StyledModal>
        </Grid>
    )
}

export default TransactionModal