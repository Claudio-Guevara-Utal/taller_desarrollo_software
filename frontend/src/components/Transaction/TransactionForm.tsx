import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import { ILaboratoryGet } from '../../interfaces/laboratoryInterface'
import { IProviderGet } from '../../interfaces/providerInterface'
import { ILaboratoryEquipmentGet } from '../../interfaces/laboratoryEquipmentInterface'
import { ITransactionPost, initialStateTransaction } from '../../interfaces/transactionInterface'
import { IAction, initialStateAction } from '../../interfaces/actionInterface' 
import * as laboratoryService from '../../services/laboratoryService'
import * as providerService from '../../services/providerService'
import * as transactionService from '../../services/transactionService'
import * as laboratoryEquipmentService from '../../services/laboratoryEquipmentService'
import toast from 'react-hot-toast'
import {
    Grid,
    TextField,
    Autocomplete,
    Button
} from '@mui/material'
import Close from '@mui/icons-material/Close'

interface Props {
    transaction: ITransactionPost
    setTransaction: Dispatch<SetStateAction<ITransactionPost>>
    action: IAction
    setAction: Dispatch<SetStateAction<IAction>>
    updateListTransactions: boolean
    setUpdateListTransactions: Dispatch<SetStateAction<boolean>>
    handleClose: () => void
}

const TransactiontForm = (props: Props) => {
    const {
        transaction,
        setTransaction,
        action,
        setAction,
        updateListTransactions,
        setUpdateListTransactions,
        handleClose
    } = props
    const [laboratories, setLaboratories] = useState<ILaboratoryGet[]>([])
    const [providers, setProviders] = useState<IProviderGet[]>([])
    const [laboratoryEquipments, setLaboratoryEquipments] = useState<ILaboratoryEquipmentGet[]>([])

    const getLaboratories = async () => {
        try {
            const data = await laboratoryService.getAllLaboratories()
            setLaboratories(data)
        } catch (error) {
            toast.error('Error al listar los laboratorios.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    const getProviders = async () => {
        try {
            const data = await providerService.getAllProviders() 
            setProviders(data)
        } catch (error) {
            toast.error('Error al listar los proveedores.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    const getLaboratoryEquipments = async () => {
        try {
            const data = await laboratoryEquipmentService.getAllLaboratoryEquipments()
            setLaboratoryEquipments(data)
        } catch (error) {
            toast.error('Error al listar los equipos de laboratorio.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }
 
    useEffect(() => {
        getLaboratories()
        getProviders()
        getLaboratoryEquipments()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (action.type === 'Crear') {
            try {
                await transactionService.createTransaction(transaction)
                setUpdateListTransactions(!updateListTransactions)
                toast.success('Transacción creada con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error: unknown) {
                const { message } = error as Error
                if (message === "Request failed with status code 400") {
                    toast.error('Error al crear la transacción. La transacción ya existe.', {
                        position: 'top-center',
                        duration: 5000
                    })
                } else {
                    toast.error('Error al crear la transacción.', {
                        position: 'top-center',
                        duration: 5000
                    })
                }
            }
        } else if (action.type === 'Editar') {
            try {
                await transactionService.updateTransaction(action.id, transaction)
                setUpdateListTransactions(!updateListTransactions)
                toast.success('Transacción actualizado con éxito.', {
                    position: 'top-center',
                    duration: 5000
                })
            } catch (error: unknown) {
                const { message } = error as Error
                if (message === "Request failed with status code 400") {
                    toast.error('Error al actualizar la transacción. La transacción ya existe.', {
                        position: 'top-center',
                        duration: 5000
                    })
                } else {
                    toast.error('Error al actualizar la transacción.', {
                        position: 'top-center',
                        duration: 5000
                    })
                }
            }
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setTransaction(initialStateTransaction)
        setAction(initialStateAction)
    }

    const resetModal = () => {
        onClickCancelar()
        handleClose()
    }

    return (
        <>
        {laboratories.length > 0 && providers.length > 0 && laboratoryEquipments.length > 0 ? (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid container justifyContent="flex-end" direction="row">
                        <Button color="primary"><Close onClick={resetModal}/></Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            options={laboratoryEquipments.map((laboratoryEquipment) => laboratoryEquipment.name)}
                            renderInput={(params) => <TextField {...params} required label="Equipo de laboratorio"/>}
                            value={transaction.laboratoryEquipment}
                            onChange={(event, laboratoryEquipment) => setTransaction({...transaction, laboratoryEquipment: laboratoryEquipment})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            options={laboratories.map((laboratory) => laboratory.name)}
                            renderInput={(params) => <TextField {...params} required label="Laboratorio"/>}
                            value={transaction.laboratory}
                            onChange={(event, laboratory) => setTransaction({...transaction, laboratory: laboratory})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            options={providers.map((provider) => provider.name)}
                            renderInput={(params) => <TextField {...params} required label="Proveedor"/>}
                            value={transaction.provider}
                            onChange={(event, provider) => setTransaction({...transaction, provider: provider})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            fullWidth
                        >
                            {action.type}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            variant="contained"
                            color="error"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default TransactiontForm