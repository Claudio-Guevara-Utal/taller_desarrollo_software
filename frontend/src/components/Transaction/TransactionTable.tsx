import DataTable from '../DataTable'
import { useState, useEffect, Dispatch, SetStateAction } from 'react'
import { ITransactionGet, ITransactionPost } from '../../interfaces/transactionInterface'
import { IAction } from '../../interfaces/actionInterface'
import HorizontalRule from '@mui/icons-material/HorizontalRule'
import { Button } from '@mui/material'
import Edit from '@mui/icons-material/Edit'
import Delete from '@mui/icons-material/Delete'
import AppRegistration from '@mui/icons-material/AppRegistration'
import List from '@mui/icons-material/List'
import * as transactionService from '../../services/transactionService'
import toast from 'react-hot-toast'

const columns: string[] = ["Equipo", "Laboratorio", "Sucursal", "Proveedor", "Opciones"]
type DataTableType = [string | any, string | any, string | any, string | any, any[]][]

interface Props {
    transactions: ITransactionGet[]
    setTransaction: Dispatch<SetStateAction<ITransactionPost>>
    setAction: Dispatch<SetStateAction<IAction>>
    updateListTransactions: boolean
    setUpdateListTransactions: Dispatch<SetStateAction<boolean>>
    handleOpen: () => void
    handleProblemModalOpen: (id: string) => void
    handleTransactionFiltered: (id: string) => void
}

const TransactionTable = (props: Props) => {
    const {
        transactions,
        setTransaction,
        setAction,
        updateListTransactions,
        setUpdateListTransactions,
        handleOpen,
        handleProblemModalOpen,
        handleTransactionFiltered
    } = props
    const [transactionsData, setTransactionsData] = useState<DataTableType>([])    

    const dataTransform = () => {
        let data: DataTableType = []
        transactions.map((transaction) => {
            data.push([
                transaction.laboratoryEquipment ? transaction.laboratoryEquipment.name : <HorizontalRule/>,
                transaction.laboratory ? transaction.laboratory.name : <HorizontalRule/>,
                transaction.laboratory ? transaction.laboratory.branchOffice ? transaction.laboratory.branchOffice.name : <HorizontalRule/> : <HorizontalRule/>,
                transaction.provider ? transaction.provider.name : <HorizontalRule/>,
                [<Button onClick={() => handleProblemModalOpen(transaction._id)} color="info"><AppRegistration/></Button>,<Button onClick={() => handleTransactionFiltered(transaction._id)} color="success"><List/></Button>,<Button onClick={() => updateTransaction(transaction)} color="warning"><Edit/></Button>, <Button onClick={() => deleteTransaction(transaction._id)} color="error"><Delete/></Button>]
            ])
        })
        setTransactionsData(data)
    }

    useEffect(() => {
        dataTransform()
    }, [transactions])

    const updateTransaction = (transaction: ITransactionGet) => {
        setTransaction({
            laboratoryEquipment: transaction.laboratoryEquipment ? transaction.laboratoryEquipment.name : '',
            laboratory: transaction.laboratory ? transaction.laboratory.name : '',
            provider: transaction.provider ? transaction.provider.name : ''
        })
        setAction({
            type: 'Editar',
            id: transaction._id
        })
        handleOpen()
    }

    const deleteTransaction = async (id: string) => {
        try {
            await transactionService.deleteTransaction(id)
            setUpdateListTransactions(!updateListTransactions)
            toast.success('Transacción eliminada con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al eliminar la transacción.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    return (
        <>
        {transactionsData.length > 0 ? (
            <DataTable
                title="Lista de transacciones"
                columns={columns}
                data={transactionsData}
            />
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default TransactionTable