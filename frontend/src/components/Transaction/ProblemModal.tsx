import { Dispatch, SetStateAction } from 'react'
import { Grid } from '@mui/material'
import { styled, Box } from '@mui/system'
import { ModalUnstyled } from '@mui/core'
import ProblemForm from './ProblemForm'
import { IProblemPost } from '../../interfaces/problemInterface'

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const style = {
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
    px: 4,
    pb: 3,
};

interface Props {
    problemModalOpen: boolean
    setProblemModalOpen: Dispatch<SetStateAction<boolean>>
    problem: IProblemPost
    setProblem: Dispatch<SetStateAction<IProblemPost>>
    transactionID: string
}

const ProblemModal = (props: Props) => {
    const {
        problemModalOpen,
        setProblemModalOpen,
        problem,
        setProblem,
        transactionID
    } = props

    return (
        <Grid container justifyContent="flex-end" direction="row">
            <StyledModal
                aria-labelledby="unstyled-modal-title"
                aria-describedby="unstyled-modal-description"
                open={problemModalOpen}
                onClose={() => setProblemModalOpen(false)}
            >
                <Box sx={style}>
                    <ProblemForm
                        setProblemModalOpen={setProblemModalOpen}
                        problem={problem}
                        setProblem={setProblem}
                        transactionID={transactionID}
                    />
                </Box>
            </StyledModal>
        </Grid>
    )
}

export default ProblemModal