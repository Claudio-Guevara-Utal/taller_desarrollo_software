import { useState, useEffect, Dispatch, SetStateAction, FormEvent } from 'react'
import { IProblemTypeGet } from '../../interfaces/problemTypeInterface'
import { IProblemPost, initialStateProblem } from '../../interfaces/problemInterface'
import { getAllProblemTypes } from '../../services/problemTypeService'
import toast from 'react-hot-toast'
import {
    Grid,
    Autocomplete,
    Button,
    TextField
} from '@mui/material'
import Close from '@mui/icons-material/Close'
import {
    DateTimePicker,
    LocalizationProvider
} from '@mui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import { createProblem } from '../../services/problemService'

interface Props {
    setProblemModalOpen: Dispatch<SetStateAction<boolean>>
    problem: IProblemPost
    setProblem: Dispatch<SetStateAction<IProblemPost>>
    transactionID: string
}

const ProblemForm = (props: Props) => {
    const {
        setProblemModalOpen,
        problem,
        setProblem,
        transactionID
    } = props
    const [problemTypes, setProblemTypes] = useState<IProblemTypeGet[]>([])

    const getProblemTypes = async () => {
        try {
            const data = await getAllProblemTypes()
            setProblemTypes(data)
        } catch (error) {
            toast.error('Error al listar los tipos de problemas.', {
                position: 'top-center',
                duration: 5000
            })
        }
    }

    useEffect(() => {
        getProblemTypes()
    }, [])

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        problem.transactionID = transactionID
        try {
            await createProblem(problem)
            toast.success('Problema creado con éxito.', {
                position: 'top-center',
                duration: 5000
            })
        } catch (error) {
            toast.error('Error al crear el problema.', {
                position: 'top-center',
                duration: 5000
            })
        }
        onClickCancelar()
    }

    const onClickCancelar = () => {
        setProblem(initialStateProblem)
    }

    const resetModal = () => {
        onClickCancelar()
        setProblemModalOpen(false)
    }

    return (
        <>
        {problemTypes.length > 0 ? (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                    <Grid container justifyContent="flex-end" direction="row">
                        <Button color="primary"><Close onClick={resetModal}/></Button>
                    </Grid>
                    <Grid item xs={12}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DateTimePicker
                                label="Fecha del problema"
                                renderInput={(params) => <TextField {...params}/>}
                                value={problem.problemDate}
                                onChange={(newDate) => setProblem({...problem, problemDate: newDate})}
                            />
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            options={problemTypes.map((problemType) => problemType.name)}
                            renderInput={(params) => <TextField {...params} required label="Tipo de Problema"/>}
                            value={problem.problemType}
                            onChange={(event, problemType) => setProblem({...problem, problemType: problemType})}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Problema"
                            placeholder="Descripción del problema"
                            fullWidth
                            multiline
                            rows={2}
                            value={problem.problemDescription}
                            onChange={({target}) => setProblem({...problem, problemDescription: target.value})}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            color="primary"
                            variant="contained"
                            fullWidth
                        >
                            Crear
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            color="error"
                            variant="contained"
                            fullWidth
                            onClick={onClickCancelar}
                        >
                            Cancelar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        ) : (
            'cargando...'
        )}
        </>
    )
}

export default ProblemForm