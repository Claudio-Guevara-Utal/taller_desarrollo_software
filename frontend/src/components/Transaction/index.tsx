import { useState, Dispatch, SetStateAction } from 'react'
import { Grid, Typography } from '@mui/material'
import TransactionModal from './TransactionModal'
import TransactionList from './TransactionList'
import ProblemModal from './ProblemModal'
import { IAction, initialStateAction } from '../../interfaces/actionInterface'
import { IProblemPost, initialStateProblem } from '../../interfaces/problemInterface'
import { ITransactionPost, initialStateTransaction } from '../../interfaces/transactionInterface'

interface Props {
    handleTransactionFiltered: (id: string) => void
}

const Transaction = (props: Props) => {
    const { handleTransactionFiltered } = props
    const [action, setAction] = useState<IAction>(initialStateAction)
    const [transaction, setTransaction] = useState<ITransactionPost>(initialStateTransaction)
    const [problem, setProblem] = useState<IProblemPost>(initialStateProblem)
    const [open, setOpen] = useState<boolean>(false)
    const [problemModalOpen, setProblemModalOpen] = useState<boolean>(false)
    const [updateListTransactions, setUpdateListTransactions] = useState<boolean>(true)
    const [transactionID, setTransactionID] = useState<string>('')

    const handleOpen = (): void => setOpen(true)
    const handleClose = (): void => setOpen(false)

    const handleProblemModalOpen = (id: string) => {
        setProblemModalOpen(true)
        setTransactionID(id)
    }

    return (
        <Grid container spacing={3}>
            <Grid item lg={6}>
                <Typography variant="h4">Transacciones</Typography>
            </Grid>
            <Grid item lg={6}>
                <TransactionModal
                    action={action}
                    setAction={setAction}
                    transaction={transaction}
                    setTransaction={setTransaction}
                    open={open}
                    handleOpen={handleOpen}
                    handleClose={handleClose}
                    updateListTransactions={updateListTransactions}
                    setUpdateListTransactions={setUpdateListTransactions}
                />
            </Grid>
            <Grid item lg={6}>
                <ProblemModal
                    problemModalOpen={problemModalOpen}
                    setProblemModalOpen={setProblemModalOpen}
                    problem={problem}
                    setProblem={setProblem}
                    transactionID={transactionID}
                />
            </Grid>
            <Grid item lg={12}>
                <TransactionList
                    setAction={setAction}
                    setTransaction={setTransaction}
                    setUpdateListTransactions={setUpdateListTransactions}
                    updateListTransactions={updateListTransactions}
                    handleOpen={handleOpen}
                    handleProblemModalOpen={handleProblemModalOpen}
                    handleTransactionFiltered={handleTransactionFiltered}
                />
            </Grid>
        </Grid>
    )
}

export default Transaction