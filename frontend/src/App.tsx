import {
    BrowserRouter,
    Routes,
    Route
} from 'react-router-dom'
import { Toaster } from 'react-hot-toast'

import Login from './pages/Login/Login'
import Dashboard from './pages/Dashboard/Dashboard'

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                    <Route path="/" element={<Login/>}/>
                    <Route path="/dashboard" element={<Dashboard/>}/>
                </Routes>
            <Toaster/>
        </BrowserRouter>
    )
}

export default App