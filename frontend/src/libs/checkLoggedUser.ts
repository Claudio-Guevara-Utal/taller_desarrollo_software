export const checkLoggedUser = () => {
    const loggedUserJSON = window.localStorage.getItem('user')
    const userParse = loggedUserJSON ? JSON.parse(loggedUserJSON) : ''
    const { token, role } = userParse

    if (token === undefined) {
        return {
            logged: false,
            role
        }
    } else {
        return {
            logged: true,
            role
        }
    }
}