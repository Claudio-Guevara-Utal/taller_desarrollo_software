import { Schema, model } from 'mongoose'

const providerStaffSchema = new Schema({
    rut: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    role: {
        type: Schema.Types.ObjectId,
        ref: 'Rol'
    }
}, {
    timestamps: true,
    versionKey: false
})

const ProviderStaff = model('ProviderStaff', providerStaffSchema)

export default ProviderStaff