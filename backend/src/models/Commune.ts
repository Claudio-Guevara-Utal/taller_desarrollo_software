import { Schema, model } from 'mongoose'

const communeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    region: {
        type: Schema.Types.ObjectId,
        ref: 'Region'
    }
}, {
    timestamps: true,
    versionKey: false
})

const Commune = model('Commune', communeSchema)

export default Commune