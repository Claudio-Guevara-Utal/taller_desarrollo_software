import { Schema, model } from 'mongoose'

const laboratorySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    branchOffice: {
        type: Schema.Types.ObjectId,
        ref: 'BranchOffice'
    }
}, {
    timestamps: true,
    versionKey: false
})

const Laboratory = model('Laboratory', laboratorySchema)

export default Laboratory