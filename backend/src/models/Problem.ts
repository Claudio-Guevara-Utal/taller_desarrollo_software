import { Schema, model } from 'mongoose'

const problemSchema = new Schema({
    problemType: {
        type: Schema.Types.ObjectId,
        ref: 'ProblemType'
    },
    problemDescription: {
        type: String,
        trim: true
    },
    problemDate: {
        type: Date
    },
    solved: {
        type: Boolean
    },
    transaction: {
        type: Schema.Types.ObjectId,
        ref: 'Transaction'
    },
    providersStaff: [
        {
            type: Schema.Types.ObjectId,
            ref: 'ProviderStaff'
        }
    ],
    staffAssignmentDate: {
        type: Date
    },
    solutionType: {
        type: Schema.Types.ObjectId,
        ref: 'SolutionType'
    },
    solutionDate: {
        type: Date
    },
}, {
    timestamps: true,
    versionKey: false
})

const Problem = model('Problem', problemSchema)

export default Problem