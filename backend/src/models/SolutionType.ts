import { Schema, model } from 'mongoose'

const solutionTypeSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique:true
    }
}, {
    timestamps: true,
    versionKey: false
})

const SolutionType = model('SolutionType', solutionTypeSchema)

export default SolutionType