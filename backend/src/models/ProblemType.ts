import { Schema, model } from 'mongoose'

const problemTypeSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique:true
    }
}, {
    timestamps: true,
    versionKey: false
})

const ProblemType = model('ProblemType', problemTypeSchema)

export default ProblemType