import { Schema, model } from 'mongoose'

const regionSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    }
}, {
    timestamps: true,
    versionKey: false
})

const Region = model('Region', regionSchema)

export default Region