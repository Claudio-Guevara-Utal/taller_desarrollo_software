import { Schema, model } from 'mongoose'

const laboratoryEquipmentSchema = new Schema({
    name: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    function: {
        type: String,
        trim: true,
        required: true
    }
}, {
    timestamps: true,
    versionKey: false
})

const LaboratoryEquipment = model('LaboratoryEquipment', laboratoryEquipmentSchema)

export default LaboratoryEquipment