import { Schema, model } from 'mongoose'

const branchOfficeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    direction: {
        type: String,
        requried: true,
        trim: true
    },
    commune: {
        type: Schema.Types.ObjectId,
        ref: 'Commune',
    },
    operative: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true,
    versionKey: false
})

const BranchOffice = model('BranchOffice', branchOfficeSchema)

export default BranchOffice