import { Schema, model } from 'mongoose'

const providerSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    direction: {
        type: String,
        required: true,
        trim: true
    },
    commune: {
        type: Schema.Types.ObjectId,
        ref: 'Commune'
    }
}, {
    timestamps: true,
    versionKey: false
})

const Provider = model('Provider', providerSchema)

export default Provider