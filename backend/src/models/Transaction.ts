import { Schema, model } from 'mongoose'

const transactionSchema = new Schema({
    laboratoryEquipment: {
        type: Schema.Types.ObjectId,
        ref: 'LaboratoryEquipment'
    },
    laboratory: {
        type: Schema.Types.ObjectId,
        ref: 'Laboratory'
    },
    problems: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Problem'
        }
    ],
    provider: {
        type: Schema.Types.ObjectId,
        ref: 'Provider'
    }
}, {
    timestamps: true,
    versionKey: false
})

const Transaction = model('Transaction', transactionSchema)

export default Transaction