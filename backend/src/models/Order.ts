import { Schema, model } from 'mongoose'

const orderSchema = new Schema({
    transaction: {
        type: Schema.Types.ObjectId,
        ref: 'Transaction'
    },
    problem: {
        type: Schema.Types.ObjectId,
        ref: 'Problem'
    },
    staffAssignmentDate: {
        type: Date
    },
    providersStaff: [
        {
            type: Schema.Types.ObjectId,
            ref: 'ProviderStaff'
        }
    ]
}, {
    timestamps: true,
    versionKey: false
})

const Order = model('Order', orderSchema)

export default Order