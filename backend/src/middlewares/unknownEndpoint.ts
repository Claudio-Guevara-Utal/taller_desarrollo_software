import { RequestHandler } from 'express'

export const unknownEndpoint: RequestHandler = (req, res) => {
    return res.status(404).json({ message: 'Unknown Endpoint.' })
}