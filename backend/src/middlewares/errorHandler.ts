import { ErrorRequestHandler } from 'express'

export const errorHandler: ErrorRequestHandler = (error, req, res, next) => {
    console.log(error.message)
    console.log(error.name)
    console.log("Hola")

    res.status(500).json({ message: error.message })

    next()
}