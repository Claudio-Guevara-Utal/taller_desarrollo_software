import { connect } from 'mongoose'
import config from '../config'

(async () => {
    try {
        const db = await connect(config.MONGO_URI)
        console.log(`Successful connection to database ${db.connection.name}`)
    } catch (error) {
        console.log(`Failed to connect database: ${error}`)
    }
})()