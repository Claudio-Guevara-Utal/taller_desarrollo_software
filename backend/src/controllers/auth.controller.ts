import { RequestHandler } from 'express'
import jwt from 'jsonwebtoken'
import config from '../config'

import RootAdministrator from '../models/RootAdministrator'
import LaboratoryAdministrator from '../models/LaboratoryAdministrador'
import ProviderAdministrator from '../models/ProviderAdministrator'
import LaboratoryStaff from '../models/LaboratoryStaff'
import ProviderStaff from '../models/ProviderStaff'
import Role from '../models/Role'

export const signIn: RequestHandler = async (req, res) => {
    const { email, password, role }  = req.body
    let user

    if (role === "Administrador Root") {
        user = await RootAdministrator.findOne({ email })
    }

    if (!user) return res.status(401).json({ message: 'Email or password wrog.' })    

    const matchPassword =  user.password === password ? true : false
    if (!matchPassword) return res.status(401).json({ message: 'Email or password wrog.' })

    const userToken = {
        id: user._id,
        email: user.email,
        name: user.name,
        lastname: user.lastname,
        role
    }

    const token = jwt.sign(userToken, config.SECRET, {
        expiresIn: 84600
    })

    res.json({token})
}

export const signUp: RequestHandler = async (req, res) => {
    const { role } = req.body
    let newUser;

    const roleFound = await Role.findOne({ name: role })

    if (role === "Administrador Root") {
        newUser = new RootAdministrator({
            ...req.body,
            password: "123",
            role: roleFound._id
        })
    } else if (role === "Administrador Laboratorio") {
        newUser = new LaboratoryAdministrator({
            ...req.body,
            password: "123",
            role: roleFound._id
        })
    } else if (role === "Administrador Proveedor") {
        newUser = new ProviderAdministrator({
            ...req.body,
            password: "123",
            role: roleFound._id
        })
    } else if (role === "Personal Laboratorio") {
        newUser = new LaboratoryStaff({
            ...req.body,
            password: "123",
            role: roleFound._id
        })
    } else if (role === "Personal Proveedor") {
        newUser = new ProviderStaff({
            ...req.body,
            password: "123",
            role: roleFound._id
        })
    }

    const savedUser = await newUser.save()
    res.status(201).json(savedUser)
}