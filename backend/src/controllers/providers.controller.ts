import { RequestHandler } from "express";
import Provider from "../models/Provider";
import Commune from "../models/Commune";

export const createProvider: RequestHandler = async (req, res) => {
    const { commune } = req.body

    const communeFound = await Commune.findOne({ name: commune })

    const newProvider = new Provider({
        ...req.body,
        commune: communeFound._id
    })

    const savedProvider = await newProvider.save()
    res.status(201).json(savedProvider)
}

export const getAllProviders: RequestHandler = async (req, res) => {
    const providers = await Provider.find({}).populate({
        path: 'commune',
        select: {
            '_id': 0,
            'name': 1
        }
    })
    res.json(providers)
}

export const deleteProvider: RequestHandler = async (req, res) => {
    const deletedProvider = await Provider.findByIdAndDelete(req.params.id)
    res.json(deletedProvider)
}

export const updateProvider: RequestHandler = async (req, res) => {
    const { commune } = req.body
    let provider = {...req.body}

    if (commune) {
        const communeFound = await Commune.findOne({ name: commune })
        provider = {
            ...provider,
            commune: communeFound._id
        }
    }

    const updatedProvider = await Provider.findByIdAndUpdate(req.params.id, provider, {
        new: true
    })
    res.json(updatedProvider)
}