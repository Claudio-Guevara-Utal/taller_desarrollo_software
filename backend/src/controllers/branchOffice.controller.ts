import { RequestHandler } from 'express'
import BranchOffice from '../models/BranchOffice'
import Commune from '../models/Commune'

export const createBranchOffice: RequestHandler = async (req, res) => {
    const { commune } = req.body

    const communeFound = await Commune.findOne({ name: commune })

    const newBranchOffice = new BranchOffice({
        ...req.body,
        commune: communeFound._id
    })

    const savedBranchOffice = await newBranchOffice.save()
    res.status(201).json(savedBranchOffice)
}

export const getAllBranchOffices: RequestHandler = async (req, res) => {
    const branchOffices = await BranchOffice.find({}).populate({
        path: 'commune',
        select: {
            '_id': 0,
            'name': 1
        }
    })
    res.json(branchOffices)
}

export const deleteBranchOffice: RequestHandler = async (req, res) => {
    const deletedBranchOffice = await BranchOffice.findByIdAndDelete(req.params.id)
    res.json(deletedBranchOffice)
}

export const updateBranchOffice: RequestHandler = async (req, res) => {
    const { commune } = req.body
    let branchOffice = {...req.body}

    if (commune) {
        const communeFound = await Commune.findOne({ name: commune })
        branchOffice = {
            ...branchOffice,
            commune: communeFound._id
        }
    }

    const updatedBranchOffice = await BranchOffice.findByIdAndUpdate(req.params.id, branchOffice, {
        new: true
    })
    res.json(updatedBranchOffice)
}