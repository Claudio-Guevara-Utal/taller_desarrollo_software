import { RequestHandler } from 'express'
import Order from '../models/Order'
import ProviderStaff from '../models/ProviderStaff'
import Problem from '../models/Problem'
import SolutionType from '../models/SolutionType'

export const getAllOrders: RequestHandler = async (req, res) => {
    const orders = await Order.find({}).populate({
        path: 'problem',
        select: {
            '_id': 1,
            'problemDescription': 1,
            'problemDate': 1,
            'solved': 1
        },
        populate: {
            path: 'problemType',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'transaction',
        select: {
            '_id': 0,
        },
        populate: {
            path: 'laboratoryEquipment',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'transaction',
        select: {
            '_id': 0
        },
        populate: {
            path: 'laboratory',
            select: {
                '_id': 0,
                'name': 0,
                'createdAt': 0,
                'updatedAt': 0
            },
            populate: {
                path: 'branchOffice',
                select: {
                    '_id': 0,
                    'name': 1
                }
            }
        }
    }).populate({
        path: 'transaction',
        select: {
            '_id': 0,
            'problems': 0,
            'createdAt': 0,
            'updatedAt': 0
        },
        populate: {
            path: 'provider',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'providersStaff',
        select: {
            '_id': 0,
            'email': 1
        }
    })
    res.json(orders) 
}

export const getAllOrderForStaff: RequestHandler = async (req, res) => {
    const orders = await Order.find({}).populate({
        path: 'transaction',
        populate: {
            path: 'laboratoryEquipment',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'transaction',
        populate: {
            path: 'laboratory',
            select: {
                '_id': 0,
                'name': 1
            },
            populate: {
                path: 'branchOffice',
                select: {
                    '_id': 0,
                    'name': 1,
                    'direction': 1
                },
                populate: {
                    path: 'commune',
                    select: {
                        '_id': 0,
                        'name': 1
                    },
                    populate: {
                        path: 'region',
                        select: {
                            '_id': 0,
                            'name': 1
                        }
                    }
                }
            }
        }
    }).populate({
        path: 'transaction',
        select: {
            '_id': 0,
            'problems': 0,
            'createdAt': 0,
            'updatedAt': 0
        },
        populate: {
            path: 'provider',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'problem',
        select: {
            '_id': 1,
            'solved': 1
        },
        populate: {
            path: 'problemType',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'providersStaff',
        select: {
            '_id': 0,
            'email': 1
        }
    })
    res.json(orders)
}

export const assignProvidersStaff: RequestHandler = async (req, res) => {
    const { orderID, problemID, providersStaff, staffAssignmentDate } = req.body

    const providersStaffFound = await ProviderStaff.find({ email: { $in: providersStaff } })

    const updatedProvidersStaff = providersStaffFound.map((staff) => staff._id)

    const order = await Order.findByIdAndUpdate(orderID, { staffAssignmentDate, providersStaff: updatedProvidersStaff }, { new: true })
    
    await Problem.findByIdAndUpdate(problemID, {
        staffAssignmentDate, 
        providersStaff: updatedProvidersStaff
    })

    res.json(order)
}

export const updateSolutionProblem: RequestHandler = async (req, res) => {
    const { problemID, solutionDate, solutionType } = req.body

    const solutionTypeFound = await SolutionType.findOne({ name: solutionType })

    const updatedProblem = await Problem.findByIdAndUpdate(problemID, {
        solutionDate,
        solutionType: solutionTypeFound._id,
        solved: true
    }, { new: true })   
    res.json(updatedProblem)
}