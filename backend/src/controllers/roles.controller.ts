import { RequestHandler } from 'express'
import Role from '../models/Role'

export const createRole: RequestHandler = async (req, res) => {
    const newRole = new Role(req.body)

    const savedRole = await newRole.save()
    res.status(201).json(savedRole)
}

export const getAllRoles: RequestHandler = async (req, res) => {
    const roles = await Role.find({})
    res.status(200).json(roles)
}