import { RequestHandler } from "express";
import Laboratory from "../models/Laboratory";
import BranchOffice from "../models/BranchOffice";

export const createLaboratory: RequestHandler = async (req, res) => {
    const { branchOffice } = req.body

    const branchOfficeFound = await BranchOffice.findOne({ name: branchOffice })

    const newLaboratory = new Laboratory({
        ...req.body,
        branchOffice: branchOfficeFound._id
    })

    const savedLaboratory = await newLaboratory.save()
    res.status(201).json(savedLaboratory)
}

export const getAllLaboratories: RequestHandler = async (req, res) => {
    const laboratories = await Laboratory.find({}).populate({
        path: 'branchOffice',
        select: {
            '_id': 0,
            'name': 1
        }
    })
    res.json(laboratories)
}

export const deleteLaboratory: RequestHandler = async (req, res) => {
    const deletedLaboratory = await Laboratory.findByIdAndDelete(req.params.id)
    res.json(deletedLaboratory)
}

export const updateLaboratory: RequestHandler = async (req, res) => {
    const { branchOffice } = req.body
    let laboratory = {...req.body}

    if (branchOffice) {
        const branchOfficeFound = await BranchOffice.findOne({ name: branchOffice })
        laboratory = {
            ...laboratory,
            branchOffice: branchOfficeFound._id
        }
    }

    const updatedLaboratory = await Laboratory.findByIdAndUpdate(req.params.id, laboratory, {
        new: true
    })
    res.json(updatedLaboratory)
}