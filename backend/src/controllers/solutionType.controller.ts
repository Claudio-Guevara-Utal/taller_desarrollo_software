import { RequestHandler } from "express";
import SolutionType from "../models/SolutionType";

export const createSolutionType: RequestHandler = async (req, res) => {
    const newSolutionType = new SolutionType(req.body)

    const savedSolutionType = await newSolutionType.save()
    res.status(201).json(savedSolutionType)
}

export const getAllSolutionTypes: RequestHandler = async (req, res) => {
    const solutionTypes = await SolutionType.find({})
    res.json(solutionTypes)
}

export const deleteSolutionType: RequestHandler = async (req, res) => {
    const deletedSolutionType = await SolutionType.findByIdAndDelete(req.params.id)
    res.json(deletedSolutionType)
}

export const updateSolutionType: RequestHandler = async (req, res) => {
    const updatedSolutionType = await SolutionType.findByIdAndUpdate(req.params.id, req.body, {
        new: true
    })
    res.json(updatedSolutionType)
}