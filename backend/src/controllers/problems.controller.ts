import { RequestHandler } from "express";
import Problem from "../models/Problem";
import ProblemType from "../models/ProblemType";
import Transaction from "../models/Transaction";
import Order from "../models/Order";

export const createProblem: RequestHandler = async (req, res) => {
    const { problemType, transactionID } = req.body

    const problemTypeFound = await ProblemType.findOne({ name: problemType })
    let transactionFound = await Transaction.findOne({ _id: transactionID })

    const newProblem = new Problem({
        ...req.body,
        problemType: problemTypeFound._id,
        solved: false,
        transaction: transactionFound._id
    })

    const savedProblem = await newProblem.save()

    if (savedProblem) {
        let transictionProblems = transactionFound.problems
        transictionProblems.push(savedProblem._id)
        await Transaction.findByIdAndUpdate(transactionFound._id, {problems: transictionProblems})
    }

    const newOrder = new Order({
        transaction: transactionFound._id,
        problem: savedProblem._id
    })
    await newOrder.save()

    res.status(201).json(savedProblem)
}

export const getAllProblems: RequestHandler = async (req, res) => {
    const problems = await Problem.find({}).populate({
        path: 'problemType',
        select: {
            '_id': 0,
            'name': 1
        }
    }).populate({
        path: 'providersStaff',
        select: {
            '_id': 0,
            'email': 1
        }
    }).populate({
        path: 'solutionType',
        select: {
            '_id': 0,
            'name': 1
        }
    }).populate({
        path: 'transaction',
        populate: {
            path: 'laboratoryEquipment',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'transaction',
        populate: {
            path: 'laboratory',
            select: {
                '_id': 0,
                'name': 0,
                'createdAt': 0,
                'updatedAt': 0
            },
            populate: {
                path: 'branchOffice',
                select: {
                    '_id': 0,
                    'name': 1
                }
            }
        }
    }).populate({
        path: 'transaction',
        select: {
            '_id': 0,
            'problems': 0,
            'createdAt': 0,
            'updatedAt': 0
        },
        populate: {
            path: 'provider',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    })
    res.json(problems)
}

export const getProblemsOfOneTransaction: RequestHandler = async (req, res) => {
    const { transactionID } = req.body

    const problems = await Problem.find({ transaction: transactionID }).populate({
        path: 'problemType',
        select: {
            '_id': 0,
            'name': 1
        }
    }).populate({
        path: 'providersStaff',
        select: {
            '_id': 0,
            'email': 1
        }
    }).populate({
        path: 'solutionType',
        select: {
            '_id': 0,
            'name': 1
        }
    }).populate({
        path: 'transaction',
        populate: {
            path: 'laboratoryEquipment',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'transaction',
        populate: {
            path: 'laboratory',
            select: {
                '_id': 0,
                'name': 0,
                'createdAt': 0,
                'updatedAt': 0
            },
            populate: {
                path: 'branchOffice',
                select: {
                    '_id': 0,
                    'name': 1
                }
            }
        }
    }).populate({
        path: 'transaction',
        select: {
            '_id': 0,
            'problems': 0,
            'createdAt': 0,
            'updatedAt': 0
        },
        populate: {
            path: 'provider',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    })
    res.json(problems)
}