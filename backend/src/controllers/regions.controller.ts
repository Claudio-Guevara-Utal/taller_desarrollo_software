import { RequestHandler } from 'express'
import Region from '../models/Region'

export const createRegion: RequestHandler = async (req, res) => {
    const newRegion = new Region(req.body)
    const savedRegion = await newRegion.save()
    res.status(201).json(savedRegion)
}

export const getAllRegions: RequestHandler = async (req, res) => {
    const regions = await Region.find({})
    res.json(regions)
}

export const deleteRegion: RequestHandler = async (req, res) => {
    const deletedRegion = await Region.findByIdAndDelete(req.params.id)
    res.json(deletedRegion)
}

export const updateRegion: RequestHandler = async (req, res) => {
    const updatedRegion = await Region.findByIdAndUpdate(req.params.id, req.body, {
        new: true
    })
    res.json(updatedRegion)
}