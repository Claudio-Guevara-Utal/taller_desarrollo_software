import { RequestHandler } from 'express'
import RootAdministrator from '../models/RootAdministrator'
import Role from '../models/Role'

export const createRootAdministrator: RequestHandler = async (req, res) => {
    const { name, lastname, email, password, role } = req.body

    const roleFound = await Role.findOne({ name: role })

    const newRootAdministrator = new RootAdministrator({
        name,
        lastname,
        email,
        password,
        role: roleFound._id
    })

    const savedRootAdministrator = await newRootAdministrator.save()
    res.status(201).json(savedRootAdministrator)
}

export const getAllRootAdministrator: RequestHandler = async (req, res) => {
    const rootAdministrators = await RootAdministrator.find({})
    res.status(200).json(rootAdministrators)
}