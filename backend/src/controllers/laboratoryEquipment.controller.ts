import { RequestHandler } from "express";
import LaboratoryEquipment from "../models/LaboratoryEquipment";

export const createLaboratoryEquipment: RequestHandler = async (req, res) => {
    const newLaboratoryEquipment = new LaboratoryEquipment(req.body)

    const savedLaboratoryEquipment = await newLaboratoryEquipment.save()
    res.status(201).json(savedLaboratoryEquipment)
}

export const getAllLaboratoryEquipments: RequestHandler = async (req, res) => {
    const laboratoryEquipments = await LaboratoryEquipment.find({})
    res.json(laboratoryEquipments)
}

export const deleteLaboratoryEquipment: RequestHandler = async (req, res) => {
    const deletedLaboratoryEquipment = await LaboratoryEquipment.findByIdAndDelete(req.params.id)
    res.json(deletedLaboratoryEquipment)
}

export const updateLaboratoryEquipment: RequestHandler = async (req, res) => {
    const updatedLaboratoryEquipment = await LaboratoryEquipment.findByIdAndUpdate(req.params.id, req.body, {
        new: true
    })
    res.json(updatedLaboratoryEquipment)
}