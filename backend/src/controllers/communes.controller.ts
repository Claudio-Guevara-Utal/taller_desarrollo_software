import { RequestHandler } from 'express'
import Commune from '../models/Commune'
import Region from '../models/Region'

export const createCommune: RequestHandler = async (req, res) => {
    const { region } = req.body

    const regionFound = await Region.findOne({ name: region })

    const newCommune = new Commune({
        ...req.body,
        region: regionFound._id
    })

    const savedCommune = await newCommune.save()
    res.status(201).json(savedCommune)
}

export const getAllCommunes: RequestHandler = async (req, res) => {
    const communes = await Commune.find({}).populate({
        path: 'region',
        select: {
            '_id': 0,
            'name': 1
        }
    })
    res.json(communes)
}

export const deleteCommune: RequestHandler = async (req, res) => {
    const deletedCommune = await Commune.findByIdAndDelete(req.params.id)
    res.json(deletedCommune)
}

export const updateCommune: RequestHandler = async (req, res) => {
    const { region } = req.body
    let commune = {...req.body}

    if (region) {
        const regionFound = await Region.findOne({ name: region })
        commune = {
            ...commune,
            region: regionFound._id
        }
    }

    const updatedCommune = await Commune.findByIdAndUpdate(req.params.id, commune, {
        new: true
    })
    res.json(updatedCommune)
}