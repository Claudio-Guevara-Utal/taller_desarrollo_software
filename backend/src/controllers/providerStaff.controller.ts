import { RequestHandler } from 'express'
import ProviderStaff from '../models/ProviderStaff'

export const getAllProvidersStaffEmail: RequestHandler = async (req, res) => {
    const providersStaffEmail = await ProviderStaff.find({}, { email: 1 }) 
    res.json(providersStaffEmail)
}