import { RequestHandler } from "express";
import ProblemType from "../models/ProblemType";

export const createProblemType: RequestHandler = async (req, res) => {
    const newProblemType = new ProblemType(req.body)

    const savedProblemType = await newProblemType.save()
    res.status(201).json(savedProblemType)
}

export const getAllProblemTypes: RequestHandler = async (req, res) => {
    const problemTypes = await ProblemType.find({})
    res.json(problemTypes)
}

export const deleteProblemType: RequestHandler = async (req, res) => {
    const deletedProblemType = await ProblemType.findByIdAndDelete(req.params.id)
    res.json(deletedProblemType)
}

export const updateProblemType: RequestHandler = async (req, res) => {
    const updatedProblemType = await ProblemType.findByIdAndUpdate(req.params.id, req.body, {
        new: true
    })
    res.json(updatedProblemType)
}