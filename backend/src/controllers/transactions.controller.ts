import { RequestHandler } from "express";
import Transaction from "../models/Transaction";
import Laboratory from "../models/Laboratory";
import BranchOffice from "../models/BranchOffice";
import Provider from "../models/Provider";
import LaboratoryEquipment from "../models/LaboratoryEquipment";

export const createTransaction: RequestHandler = async (req, res) => {
    const {
        laboratoryEquipment,
        laboratory,
        provider
    } = req.body

    const laboratoryEquipmentFound = await LaboratoryEquipment.findOne({ name: laboratoryEquipment })
    const laboratoryFound = await Laboratory.findOne({ name: laboratory })
    const providerFound = await Provider.findOne({ name: provider })

    const transactionFound = await Transaction.findOne({
        laboratoryEquipment: laboratoryEquipmentFound._id,
        laboratory: laboratoryFound._id,
        provider: providerFound._id
    })

    if (transactionFound) {
        return res.status(400).json({ message: 'La transacción ya existe.' })
    }

    const newTransaction = new Transaction({
        laboratoryEquipment: laboratoryEquipmentFound._id,
        laboratory: laboratoryFound._id,
        provider: providerFound._id
    })

    const savedTransaction = await newTransaction.save()
    res.status(201).json(savedTransaction)
}

export const getAllTransactions: RequestHandler = async (req, res) => {
    const transactions = await Transaction.find({}).populate({
        path: 'laboratoryEquipment',
        select: {
            '_id': 0,
            'name': 1
        }
    }).populate({
        path: 'laboratory',
        select: {
            '_id': 0,
            'name': 1
        },
        populate: {
            path: 'branchOffice',
            select: {
                '_id': 0,
                'name': 1
            }
        }
    }).populate({
        path: 'provider',
        select: {
            '_id': 0,
            'name': 1
        }
    })
    res.json(transactions)
}

export const deleteTransaction: RequestHandler = async (req, res) => {
    const deletedTransaction = await Transaction.findByIdAndDelete(req.params.id)
    res.json(deletedTransaction)
}

export const updateTransaction: RequestHandler = async (req, res) => {
    const {
        laboratoryEquipment,
        laboratory,
        provider
    } = req.body
    let transaction = {...req.body}
    let laboratoryEquipmentFound;
    let laboratoryFound;
    let providerFound

    if (laboratoryEquipment) {
        laboratoryEquipmentFound = await LaboratoryEquipment.findOne({ name: laboratoryEquipment })
    }
    
    if (laboratory) {
        laboratoryFound = await Laboratory.findOne({ name: laboratory })
    }

    if (provider) {
        providerFound = await Provider.findOne({ name: provider })
    }

    const transactionFound = await Transaction.findOne({
        laboratoryEquipment: laboratoryEquipmentFound._id,
        laboratory: laboratoryFound._id,
        provider: providerFound._id
    })

    if (transactionFound) return res.status(400).json({ message: 'La transacción ya existe.' })

    transaction = {
        ...transaction,
        laboratoryEquipment: laboratoryEquipmentFound._id,
        laboratory: laboratoryFound._id,
        provider: providerFound._id
    }

    const updatedTransaction = await Transaction.findByIdAndUpdate(req.params.id, transaction, {
        new: true
    })
    res.json(updatedTransaction)
}