import dotenv from 'dotenv'

dotenv.config()

const config = {
    PORT: process.env.PORT,
    MONGO_URI: process.env.MONGO_URI || 'clinica-bupa',
    SECRET: process.env.SECRET || '123'
}

export default config