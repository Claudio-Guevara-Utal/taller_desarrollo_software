import Role from "../models/Role";
import Region from "../models/Region";

export const createRoles = async () => {
    const count = await Role.estimatedDocumentCount()

    if (count > 0) return

    await Promise.all([
        new Role({ name: "Administrador Root" }).save(),
        new Role({ name: "Administrador Laboratorio" }).save(),
        new Role({ name: "Administrador Proveedor" }).save(),
        new Role({ name: "Personal Laboratorio" }).save(),
        new Role({ name: "Personal Proveedor" }).save()
    ])
}

export const createRegions = async () => {
    const count = await Region.estimatedDocumentCount()

    if (count > 0) return

    await Promise.all([
        new Region({ name: 'Arica-Parinacota' }).save(),
        new Region({ name: 'Tarapacá' }).save(),
        new Region({ name: 'Antofagasta' }).save(),
        new Region({ name: 'Atacama' }).save(),
        new Region({ name: 'Coquimbo' }).save(),
        new Region({ name: 'Valparaíso' }).save(),
        new Region({ name: 'Metropolitana' }).save(),
        new Region({ name: `O'Higgins` }).save(),
        new Region({ name: 'Maule' }).save(),
        new Region({ name: 'Ñuble' }).save(),
        new Region({ name: 'Bío Bío' }).save(),
        new Region({ name: 'Araucanía' }).save(),
        new Region({ name: 'Los Ríos' }).save(),
        new Region({ name: 'Los Lagos' }).save(),
        new Region({ name: 'Aysén' }).save(),
        new Region({ name: 'Magallanes y Antártica Chilena' }).save(),
    ])
}
