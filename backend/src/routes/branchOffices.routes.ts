import { Router } from "express";
import * as branchOfficeController from '../controllers/branchOffice.controller'

const router = Router()

router.post('/', branchOfficeController.createBranchOffice)
router.get('/', branchOfficeController.getAllBranchOffices)
router.delete('/:id', branchOfficeController.deleteBranchOffice)
router.put('/:id', branchOfficeController.updateBranchOffice)

export default router