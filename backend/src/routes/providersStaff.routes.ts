import { Router } from 'express'
import * as providerStaffController from '../controllers/providerStaff.controller'

const router = Router()

router.get('/emails', providerStaffController.getAllProvidersStaffEmail)

export default router