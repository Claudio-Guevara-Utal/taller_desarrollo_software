import { Router } from "express";

import * as rootAdministratorController from '../controllers/rootAdministrators.controller'

const router = Router()

router.post('/', rootAdministratorController.createRootAdministrator)
router.get('/', rootAdministratorController.getAllRootAdministrator)

export default router