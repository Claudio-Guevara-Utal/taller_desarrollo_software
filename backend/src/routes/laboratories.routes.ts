import { Router } from "express";
import * as laboratoriesController from '../controllers/laboratories.controller'

const router = Router()

router.post('/', laboratoriesController.createLaboratory)
router.get('/', laboratoriesController.getAllLaboratories)
router.delete('/:id', laboratoriesController.deleteLaboratory)
router.put('/:id', laboratoriesController.updateLaboratory)

export default router