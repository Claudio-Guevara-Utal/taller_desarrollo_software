import { Router } from "express";
import * as problemsController from '../controllers/problems.controller'

const router = Router()

router.post('/', problemsController.createProblem)
router.get('/', problemsController.getAllProblems)
router.post('/one-transaction', problemsController.getProblemsOfOneTransaction)

export default router