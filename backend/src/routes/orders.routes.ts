import { Router } from "express";
import * as ordersController from '../controllers/orders.controller'

const router = Router()

router.get('/', ordersController.getAllOrders)
router.get('/staffs', ordersController.getAllOrderForStaff)
router.post('/assign', ordersController.assignProvidersStaff)
router.post('/solution', ordersController.updateSolutionProblem)

export default router