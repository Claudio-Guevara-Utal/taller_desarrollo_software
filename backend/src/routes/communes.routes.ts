import { Router } from "express";

import * as communeController from '../controllers/communes.controller'

const router = Router()

router.post('/', communeController.createCommune)
router.get('/', communeController.getAllCommunes)
router.delete('/:id', communeController.deleteCommune)
router.put('/:id', communeController.updateCommune)

export default router