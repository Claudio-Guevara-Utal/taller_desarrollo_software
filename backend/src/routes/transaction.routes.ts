import { Router } from "express";
import * as transactionController from '../controllers/transactions.controller'

const router = Router()

router.post('/', transactionController.createTransaction)
router.get('/', transactionController.getAllTransactions)
router.delete('/:id', transactionController.deleteTransaction)
router.put('/:id', transactionController.updateTransaction)

export default router