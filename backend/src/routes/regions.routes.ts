import { Router } from "express";

import * as regionsCtrl from '../controllers/regions.controller'

const router = Router()

router.post('/', regionsCtrl.createRegion)
router.get('/', regionsCtrl.getAllRegions)
router.delete('/:id', regionsCtrl.deleteRegion)
router.put('/:id', regionsCtrl.updateRegion)

export default router