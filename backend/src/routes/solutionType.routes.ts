import { Router } from "express";
import * as solutionTypeController from '../controllers/solutionType.controller'

const router = Router()

router.get('/', solutionTypeController.getAllSolutionTypes)
router.post('/', solutionTypeController.createSolutionType)
router.delete('/:id', solutionTypeController.deleteSolutionType)
router.put('/:id', solutionTypeController.updateSolutionType)

export default router