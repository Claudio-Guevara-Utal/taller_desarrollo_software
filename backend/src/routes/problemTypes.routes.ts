import { Router } from "express";
import * as problemTypesController from '../controllers/problemTypes.controller'

const router = Router()

router.post('/', problemTypesController.createProblemType)
router.get('/', problemTypesController.getAllProblemTypes)
router.delete('/:id', problemTypesController.deleteProblemType)
router.put('/:id', problemTypesController.updateProblemType)

export default router