import { Router } from "express";
import * as laboratoryEquipmentController from '../controllers/laboratoryEquipment.controller'

const router = Router()

router.post('/', laboratoryEquipmentController.createLaboratoryEquipment)
router.get('/', laboratoryEquipmentController.getAllLaboratoryEquipments)
router.delete('/:id', laboratoryEquipmentController.deleteLaboratoryEquipment)
router.put('/:id', laboratoryEquipmentController.updateLaboratoryEquipment)

export default router