import { Router } from "express";
import * as rolesController from '../controllers/roles.controller'

const router = Router()

router.post('/', rolesController.createRole)
router.get('/', rolesController.getAllRoles)

export default router