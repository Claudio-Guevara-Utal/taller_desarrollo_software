import { Router } from 'express'
import * as providersController from '../controllers/providers.controller'

const router = Router()

router.post('/', providersController.createProvider)
router.get('/', providersController.getAllProviders)
router.delete('/:id', providersController.deleteProvider)
router.put('/:id', providersController.updateProvider)

export default router