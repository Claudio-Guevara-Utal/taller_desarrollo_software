import rolesRoutes from './roles.routes'
import rootAdministratorsRoutes from './rootAdministrator.routes'
import authRoutes from './auth.routes'
import regionsRoutes from './regions.routes'
import communeRoutes from './communes.routes'
import branchOfficeRoutes from './branchOffices.routes'
import laboratoryRoutes from './laboratories.routes'
import transactionRoutes from './transaction.routes'
import problemTypeRoutes from './problemTypes.routes'
import problemRoutes from './problems.routes'
import providerRoutes from './providers.routes'
import orderRoutes from './orders.routes'
import providerStaffRoutes from './providersStaff.routes'
import solutionTypeRoutes from './solutionType.routes'
import laboratoryEquipmentRoutes from './laboratoryEquipment.routes'

export {
    rolesRoutes,
    rootAdministratorsRoutes,
    authRoutes,
    regionsRoutes,
    communeRoutes,
    branchOfficeRoutes,
    laboratoryRoutes,
    transactionRoutes,
    problemTypeRoutes,
    problemRoutes,
    providerRoutes,
    orderRoutes,
    providerStaffRoutes,
    solutionTypeRoutes,
    laboratoryEquipmentRoutes
}