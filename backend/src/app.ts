import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import helmet from 'helmet'
import 'express-async-errors'

import * as routes from './routes'
import * as middlewares from './middlewares'

import {
    createRoles,
    createRegions,
}  from './libs/initialSetup'

const app = express()

/* Initial Setup */
createRoles()
createRegions()

app.use(morgan('dev'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(helmet())

app.use('/api/roles', routes.rolesRoutes)
app.use('/api/root-administrators', routes.rootAdministratorsRoutes)
app.use('/api/auth', routes.authRoutes)
app.use('/api/regions', routes.regionsRoutes)
app.use('/api/communes', routes.communeRoutes)
app.use('/api/branch-offices', routes.branchOfficeRoutes)
app.use('/api/laboratories', routes.laboratoryRoutes)
app.use('/api/transactions', routes.transactionRoutes)
app.use('/api/problem-types', routes.problemTypeRoutes)
app.use('/api/problems', routes.problemRoutes)
app.use('/api/providers', routes.providerRoutes)
app.use('/api/orders', routes.orderRoutes)
app.use('/api/providers-staff', routes.providerStaffRoutes)
app.use('/api/solution-types', routes.solutionTypeRoutes)
app.use('/api/laboratory-equipments', routes.laboratoryEquipmentRoutes)

app.use(middlewares.unknownEndpoint)
app.use(middlewares.errorHandler)

export default app